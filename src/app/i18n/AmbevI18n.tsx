import React, { createContext, useContext, useMemo } from "react";

const I18N_CONFIG_KEY = "i18nConfig";
const initialState = {
  selectedLang: "pt"
};

function getConfig() {
  const ls = localStorage.getItem(I18N_CONFIG_KEY);
  if (ls) {
    try {
      return JSON.parse(ls);
    } catch (er) {
      console.error(er);
    }
  }
  return initialState;
}

// Side effect
export function setLanguage(lang: string) {
  localStorage.setItem(I18N_CONFIG_KEY, JSON.stringify({ selectedLang: lang }));
  window.location.reload();
}

const I18nContext: React.Context<any> = createContext('');

export function useLang() {
  return useContext(I18nContext).selectedLang;
}

export const I18nConsumer = I18nContext.Consumer;

export function AmbevI18nProvider({ children }: JSX.ElementChildrenAttribute) {
  const lang = useMemo(getConfig, []);

  return <I18nContext.Provider value={lang}> {children} </I18nContext.Provider>;
}
