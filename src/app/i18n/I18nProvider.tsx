import React from "react";
import { useLang } from "./AmbevI18n";
import { IntlProvider } from "react-intl";

import ptMessages from "./messages/pt.json";
import esMessages from "./messages/es.json";

const allMessages: any = {
  es: esMessages,
  pt: ptMessages
};

export function I18nProvider({ children }: any) {
  const locale = useLang();
  const messages = allMessages[locale];

  return (
    <IntlProvider locale={locale} messages={messages}>
      {children}
    </IntlProvider>
  );
}
