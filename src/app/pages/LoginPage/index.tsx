/* eslint-disable jsx-a11y/anchor-is-valid */
import ambevLogoImg from "../../../assets/ambev-logo.svg";
import logoComiteImg from "../../../assets/logo-comite.png";
import emailIconImg from "../../../assets/email-icon.png";

import { HiEye, HiEyeOff } from "react-icons/hi";

import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useAuth } from "../../shared/hooks/useAuth";

import { useTranslation } from "react-i18next";

export function LoginPage() {
  const { t } = useTranslation();

  const auth = useAuth();
  const history = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);

  const handleClick = (e: any) => {
    e.preventDefault();
    setShow(!show);
  };

  function handleEmailChange(event: any) {
    setEmail(event.target.value);
  }
  function handlePasswordChange(event: any) {
    setPassword(event.target.value);
  }
  function onFinish() {
    auth
      .authenticate(email, password)
      .then(() => {
        history("/Home");
      })
      .catch((err) => {
        alert("E-mail ou senha incorreta");
      });
  }

  return (
    <>
      <div className="bg-background-image bg flex items-center justify-center">
        <div className="py-8 flex items-center md:h-screen">
          <div className="flex md:px-10 px-2 flex-col flex-wrap max-w-[67rem] mx-auto justify-center">
            <div
              className="md:mb-32 pt-5 mb-5 flex items-end justify-end
             flex-col-reverse md:flex-row relative w-full"
            >
              <div className="w-full lg:w-1/2">
                <div
                  className="bg-slate-50 shadow-[rgb(0,0,0,.12)] max-w-sm
               p-3 xl:p-5 rounded-lg min-w-fit"
                >
                  <div className="mb-8 mt-8 w-full px-3 flex items-center justify-center">
                    <a href="#">
                      <img
                        src={logoComiteImg}
                        alt="Logo Comitê de Gente"
                        width="270"
                      />
                    </a>
                  </div>
                  <div>
                    <div className="flex flex-wrap">
                      <div className="w-full relative">
                        <input
                          name="email"
                          placeholder="Seu email"
                          type="email"
                          value={email}
                          onChange={handleEmailChange}
                          className="my-5 w-full p-1 border border-gray-300 rounded-md 
                          mt-1 h-12 shadow-sm text-sm"
                        />
                        <button
                          type="button"
                          className="absolute right-3 top-[18px] "
                        >
                          <img
                            src={emailIconImg}
                            alt="email-icon"
                            className="text-gray-300 opacity-60 mt-1"
                          />
                        </button>
                      </div>
                    </div>
                    <div className="flex flex-wrap">
                      <div className="w-full relative">
                        <input
                          name="password"
                          placeholder="Sua senha"
                          type={show ? "text" : "password"}
                          onChange={handlePasswordChange}
                          value={password}
                          className="w-full p-1 border border-gray-300 rounded mt-1 h-12"
                        />
                        <button
                          type="button"
                          className="absolute right-3 top-[18px] text-gray-400"
                        >
                          <div>
                            {show ? (
                              <HiEye size={20} onClick={handleClick} />
                            ) : (
                              <HiEyeOff size={20} onClick={handleClick} />
                            )}
                          </div>
                        </button>
                      </div>
                    </div>
                    <div className="mt-4 text-center">
                      {/* Remember */}
                      <div className="mt-6 flex items-center justify-center">
                        <input
                          id="remember"
                          type="checkbox"
                          className="h-4 rounded"
                        />
                        <label
                          htmlFor="remember"
                          className="ml-2 text-sm text-gray-600 cursor-pointer"
                        >
                          Lembrar meu email e senha
                        </label>
                      </div>
                      <button
                        onClick={onFinish}
                        className="h-12 my-6 w-full py-2 px-4 bg-blue-600
            hover:bg-blue-700 rounded-md text-white text-sm"
                      >
                        Fazer Login
                      </button>
                    </div>
                    <div className="text-center text-gray-400 p-3">
                      Esqueceu sua senha?
                      <a
                        href="/ForgetPassword"
                        className="font-medium text-sm text-blue-500 underline hover:no-underline ml-2"
                      >
                        Clique aqui
                      </a>
                    </div>
                  </div>
                </div>
                <a href="#" className="mt-10 flex md:hidden justify-center">
                  <img src={ambevLogoImg} alt="logo ambev" width="100" />
                </a>
              </div>

              <div
                className="lg:pl-28 lg:w-1/2 ml-3 w-full h-full flex justify-end 
            flex-col lg:flex-col"
              >
                <div className="flex flex-col">
                  <h1 className="text-white font-extrabold text-3xl block mb-6">
                    {t("LOGIN.HELLO")}
                  </h1>

                  <p className="text-white text-lg font-normal block mb-2">
                    {t("LOGIN.BEST_PRACTICES_COMMITTEE")}
                  </p>
                  <p className="text-white text-lg font-normal mb-5 md:mb-0 pr-16 md:pr-0">
                    Aqui você poderá acompanhar o desempenho da sua unidade,
                    compartilhar suas conquistas e interagir com seus colegas.
                  </p>
                </div>
                <a href="#" className="mt-10 lg:mt-16 hidden md:flex">
                  <img src={ambevLogoImg} alt="logo ambev" width="100" />
                </a>
              </div>
            </div>
            <div
              className="flex sm:flex-row flex-col lg:flex-row justify-between
          lg:justify-between text-white border-t pt-1 border-white"
            >
              <span className="font-medium">ajuda.mp@ambev.com.br</span>
              <span className="font-medium">+55 11 9999-9999</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
