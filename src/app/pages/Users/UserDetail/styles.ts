import tw from "tailwind-styled-components";

export const Subtitle = tw.p` 
  text-xl 
  text-gray-600 
  font-semibold 
  mb-1
`
export const Text = tw.p` 
  text-gray-400 
  font-normal 
  text-base
`
export const TextBold = tw.span` 
  font-semibold 
  text-gray-700
`
export const InputsContainer = tw.div` 
  flex 
  items-center 
  flex-col 
  justify-center 
  my-10
`
export const TextAreaContainer = tw.div` 
  mt-7
`
export const Label = tw.label`  
  text-sm 
  font-medium 
  text-gray-700 
  flex 
  justify-between
`
export const Textarea = tw.textarea` 
  max-h-32 
  w-96 
  p-3 
  shadow-sm 
  mt-1 
  focus:ring-indigo-500 
  focus:border-indigo-500
  block  
  sm:text-sm 
  border-gray-300 
  rounded-md border
`
export const InputAvatarContainer = tw.div` 
  flex 
  mt-7 
  w-96
`
export const AvatarImage = tw.img` 
  mx-auto 
  h-24 
  w-24 
  my-3
  rounded-full 
`
export const DeleteImage = tw.button` 
  text-gray-400 
  underline 
  flex 
  items-center 
  justify-center
`
export const InputFileContainer = tw.div` 
  max-w-lg 
  w-72 
  h-36 
  mt-5 
  ml-3 
  flex 
  justify-center 
  pt-5 
  pb-6 
  border-2
  border-gray-300 
  border-dashed 
  rounded-md
`
export const InputFileWrapper = tw.div` 
  space-y-1 
  text-center 
  items-center 
  flex 
  flex-col 
  justify-center
`
export const SVG = tw.svg` 
  mx-auto 
  h-12 
  w-12 
  text-gray-400
`
export const InputFileTextContainer = tw.div` 
  flex 
  text-sm 
  text-gray-600
`
export const InputFilePointer = tw.div` 
  relative 
  cursor-pointer 
  bg-white 
  rounded-md 
  ont-medium 
  text-indigo-600 
  hover:text-indigo-500 
  focus-within:outline-none 
  focus-within:ring-2 
  focus-within:ring-offset-2 
  focus-within:ring-indigo-500
`
export const ImgFormats = tw.p` 
  text-xs 
  text-gray-500
`