import { useLocation } from "react-router-dom";
import { FormEvent, useState } from "react";

import { Header } from "../../../shared/components/Header";
import { setPreviewImage } from "../../../utils/setPreviewImage";
import { PROFILES_SELECT, TUser } from "../../../shared/MOCK/users2";
import { UNITIES, UNITIES_SELECT } from "../../../shared/MOCK/unities2";
import { FRONTS, FRONTS_SELECT } from "../../../shared/MOCK/fronts2";

import InputText from "../../../shared/components/InputText";
import InputSelect from "../../../shared/components/InputSelect";
import DetailsCardHeader from "../../../shared/components/DetailsCardHeader";
import LineSeparator from "../../../shared/components/LineSeparator";
import CreateSaveButton from "../../../shared/components/CreateSaveButton";
import { AvatarImage, DeleteImage, InputAvatarContainer, InputFileContainer, InputFilePointer, InputFileTextContainer, InputFileWrapper, InputsContainer, Label, Subtitle, SVG, Text, Textarea, TextAreaContainer, TextBold, ImgFormats } from "./styles";

interface IState {
  person: TUser
}

export function UserDetail() {
  const location = useLocation()
  const state = location.state as IState
  const person = state?.person
  const initialAvatar = person?.image ? person.image : 'https://symphonyintheflinthills.org/wp-content/uploads/2018/01/profile-placeholder-300x300.png'

  const [frontValue, setFrontValue] = useState(FRONTS[person?.front]?.name)
  const [unityValue, setUnityValue] = useState(UNITIES[person?.unity]?.unity)
  const [profileValue, setProfileValue] = useState(person?.role)
  const [avatar, setAvatar] = useState(initialAvatar)
  const handleAvatarChange = (event: FormEvent) => {
    const inputFile = event.target as HTMLInputElement;
    const { files } = inputFile;
    if (files) {
      setPreviewImage(files, setAvatar);
    }
  };

  return (
    <div className="min-h-full">
      {/* Header */}
      <Header />
      {/*  */}
      <main className="-mt-24">
        <div className="max-w-7xl mx-auto pb-24 px-4 sm:px-6 lg:px-8">
          <div className="bg-white h-full rounded-lg shadow px-5 py-8 sm:px-6">

            <DetailsCardHeader createNew={!person} texts={['Novo Usuário', 'Detalhes do Usuário']} />

            <div>
              <Subtitle>Informações básicas do Usuário</Subtitle>
              <Text>
                Informe os dados pessoais,{" "}
                <TextBold>Unidade</TextBold>
                {" "}e{" "}
                <TextBold>Perfil</TextBold>
                . Caso o usuário faça parte de uma{" "}
                <TextBold>Frente</TextBold>
                , informe nesse painel.
              </Text>
              <LineSeparator />
            </div>
            <InputsContainer>
              <InputText label='Nome' defaultValue={person?.name} />
              <InputText label='Email' defaultValue={person?.email} />
              <InputSelect
                title="Frente"
                data={FRONTS_SELECT}
                filterValue={frontValue}
                setFilterValue={setFrontValue}
                placeholder={'Selecione uma frente caso o usuário faça parte de alguma'}
                optional
              />
              <InputSelect
                title="Unidade"
                data={UNITIES_SELECT}
                filterValue={unityValue}
                setFilterValue={setUnityValue}
              />
              <InputSelect
                title="Perfil"
                data={PROFILES_SELECT}
                filterValue={profileValue}
                setFilterValue={setProfileValue}
              />
              <TextAreaContainer>
                <Label htmlFor="comment">
                  Apresentação<span>(Opcional)</span>
                </Label>
                <Textarea
                  placeholder="Escreva um pequeno texto de apresentação sobre você e sua unidade!"
                  rows={4}
                  name="comment"
                  id="comment"
                  defaultValue={""}
                />
              </TextAreaContainer>
              <InputAvatarContainer>
                <div className="flex-col">
                  <Label htmlFor="file-upload">Foto de Perfil</Label>
                  <AvatarImage src={avatar} alt="Foto de Perfil" />
                  <DeleteImage type='button'>Excluir foto</DeleteImage>
                </div>
                <InputFileContainer>
                  <InputFileWrapper>
                    <SVG
                      stroke="currentColor"
                      fill="none"
                      viewBox="0 0 48 48"
                      aria-hidden="true"
                    >
                      <path
                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                        strokeWidth={2}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </SVG>
                    <InputFileTextContainer>
                      <InputFilePointer>
                        <span>Selecione um arquivo</span>
                        <input
                          id="file-upload"
                          name="file-upload"
                          type="file"
                          className="sr-only"
                          onChange={handleAvatarChange}
                        />
                      </InputFilePointer>
                      <p className="pl-1">ou arraste até aqui</p>
                    </InputFileTextContainer>
                    <ImgFormats>
                      PNG, JPG, GIF up to 10MB
                    </ImgFormats>
                  </InputFileWrapper>
                </InputFileContainer>
              </InputAvatarContainer >

            </InputsContainer >
            <LineSeparator />
            <CreateSaveButton type="Usuário" create={!person} />
          </div >
        </div >
      </main >
    </div >
  );
}
