export const style_Td = `
  px-6 
  py-4 
  whitespace-nowrap 
  text-sm 
`;
export const style_TdAction = `
  text-gray-500 
  text-right 
  font-medium
`;
export const style_EditLink = `
  text-indigo-600
  hover:text-indigo-900
`;