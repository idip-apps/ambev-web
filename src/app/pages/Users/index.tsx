import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import CrudCardHeader from "../../shared/components/CrudCardHeader";
import FilterContainer from "../../shared/components/FilterContainer";
import { Header } from "../../shared/components/Header";
import InputSelect from "../../shared/components/InputSelect";
import InputText from "../../shared/components/InputText";
import { Pagination } from "../../shared/components/Pagination";
import TableContainer from "../../shared/components/TableContainer";
import TableDataUnity from "../../shared/components/TableDataUnity";
import TableDataUser from "../../shared/components/TableDataUser";
import { UNITIES, UNITIES_SELECT } from "../../shared/MOCK/unities2";
import { PROFILES_SELECT, TUser, USERS } from "../../shared/MOCK/users2";

import { style_EditLink, style_Td, style_TdAction } from "./styles";

const usersPerPage = 6;
export function Users() {
  const [filterChecked, setFilterChecked] = useState(false);
  const inputUser = useRef<HTMLInputElement>(null!)
  const [unityValue, setUnityValue] = useState('')
  const [profileValue, setProfileValue] = useState('')
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [numberOfPages, setNumberOfPages] = useState<number>(
    Math.ceil(USERS.length / usersPerPage)
  );
  const [usersList, setUsersList] = useState(USERS);
  const [visibleUsers, setVisibleUsers] = useState(
    usersList.slice(0, usersPerPage)
  );

  const updateList = (list: TUser[]) => {
    setUsersList(list);
    setNumberOfPages(Math.ceil(list.length / usersPerPage));
    setVisibleUsers(list.slice(0, usersPerPage));
  };

  const handleInputChange = () => {
    if (!filterChecked) return
    const nameValue = inputUser.current.value
    const newList = USERS.filter(user => {
      let show = true
      if (nameValue) {
        show = user.name.toLowerCase().includes(nameValue.toLowerCase());
      }
      if (unityValue) {
        show = show && (UNITIES[user.unity].unity === unityValue)
      }
      if (profileValue) {
        show = show && user.role === profileValue;
      }
      return show;
    });
    updateList(newList);
  };

  const handlePageClick = async (pageNumber: number) => {
    // when connect to api change the logic to get admins from backend
    if (pageNumber < 1 || pageNumber > numberOfPages) return;
    setCurrentPage(pageNumber);
    const offset = (pageNumber - 1) * usersPerPage;
    setVisibleUsers(usersList.slice(offset, offset + usersPerPage));
  };

  useEffect(() => {
    if (!filterChecked) updateList(USERS);
    handleInputChange();
  }, [filterChecked, unityValue, profileValue]);

  return (
    <div className="min-h-full ">
      {/* Header */}
      <Header />
      {/*  */}

      <main className="-mt-24 ">
        <div className="max-w-7xl mx-auto pb-24 px-4 sm:px-6 lg:px-8">
          <div className="bg-white rounded-lg shadow px-5 py-8 sm:px-6">
            <CrudCardHeader createNewHref="/userdetail" title="Usuários" />
            <FilterContainer
              filterChecked={filterChecked}
              setFilterChecked={setFilterChecked}
            >
              <InputText
                handleInputChange={handleInputChange}
                label='Usuário'
                placeholder="Nome do Usuário"
                ref={inputUser}
              />
              <InputSelect
                title="Unidade"
                data={UNITIES_SELECT}
                filterValue={unityValue}
                setFilterValue={setUnityValue}
              />
              <InputSelect
                title="Perfil"
                data={PROFILES_SELECT}
                filterValue={profileValue}
                setFilterValue={setProfileValue}
              />
            </FilterContainer>
            <TableContainer
              headers={['Nome', 'Unidade', 'Perfil']}
              SrOnlyHeaders={['Editar']}
            >
              {visibleUsers.map((person) => (
                <tr key={person.id}>
                  <td className={style_Td}>
                    <TableDataUser user={person} />
                  </td>
                  <td className={style_Td}>
                    <TableDataUnity unity={UNITIES[person.unity]} />
                  </td>

                  <td className={`${style_Td} text-gray-500`}>
                    {person.role}
                  </td>
                  <td className={`${style_Td} ${style_TdAction}`}>
                    <Link to="/userdetail"
                      className={style_EditLink}
                      state={{ person }}
                    >
                      Detalhes
                    </Link>
                  </td>
                </tr>
              ))}
            </TableContainer>
            <Pagination
              current={currentPage}
              numberOfPages={numberOfPages}
              onClickPage={handlePageClick}
            />
          </div>
        </div>
      </main>
    </div>
  );
}
