import { useLocation } from "react-router-dom";
import { useState } from "react";

import { Header } from "../../../shared/components/Header";
import { TFront } from "../../../shared/MOCK/fronts2";
import { MANAGERS_SELECT, USERS } from "../../../shared/MOCK/users2";
import InputSelect from "../../../shared/components/InputSelect";

import InputText from "../../../shared/components/InputText";
import DetailsCardHeader from "../../../shared/components/DetailsCardHeader";
import LineSeparator from "../../../shared/components/LineSeparator";
import CreateSaveButton from "../../../shared/components/CreateSaveButton";
import { InputsContainer, Subtitle, Text, TextBold } from "./styles";


interface IState {
  front: TFront
}

export function FrontsDetail() {
  const location = useLocation()
  const state = location.state as IState
  const front = state?.front
  const [managerValue, setManagerValue] = useState<string>(USERS[front?.manager]?.name)
  return (
    <div className="min-h-full">
      {/* Header */}
      <Header />
      {/*  */}

      <main className="-mt-24">
        <div className="max-w-7xl mx-auto pb-24 px-4 sm:px-6 lg:px-8">
          <div className="bg-white  rounded-lg shadow px-5 py-8 sm:px-6">

            <DetailsCardHeader
              createNew={!front}
              texts={['Nova Frente', 'Detalhes da Frente']}
            />
            <Subtitle>
              Informações básicas da Frente
            </Subtitle>
            <Text>
              Informe um nome para a{" "}
              <TextBold>Frente</TextBold>
              {" "}e seu{" "}
              <TextBold>Responsável</TextBold>
              . Participantes são adicionados{" "}
              <TextBold>automaticamente</TextBold>
              .
            </Text>
            <LineSeparator />
            <InputsContainer>
              <InputText
                label="Nome da Frente"
                defaultValue={front?.name} />
              <InputSelect data={MANAGERS_SELECT}
                filterValue={managerValue}
                setFilterValue={setManagerValue}
                title={'Gerente da Gente (Responsável)'} />
            </InputsContainer>
            <LineSeparator />
            <CreateSaveButton type="Frente" create={!front} />

          </div>
        </div>
      </main>
    </div>
  );
}
