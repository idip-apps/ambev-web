import tw from "tailwind-styled-components";

export const style_Td = `
  px-6 
  py-4 
  whitespace-nowrap 
  text-sm 
`;
export const ParticipantsImgsContainer = tw.div`
  flex 
  -space-x-4 
  relative 
  z-0 
  overflow-hidden
`;
export const ParticipantImg = tw.img`
  relative 
  z-30 
  inline-block 
  h-8 
  w-8 
  rounded-full 
  ring-2 
  ring-white
`;
export const style_TdAction = `
  text-gray-500 
  text-right 
  font-medium
`;
export const style_EditLink = `
  text-indigo-600
  hover:text-indigo-900
`;
