import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";

import { Header } from "../../shared/components/Header";
import { Pagination } from "../../shared/components/Pagination";
import { FRONTS, TFront } from "../../shared/MOCK/fronts2";
import { MANAGERS_SELECT, USERS } from "../../shared/MOCK/users2";
import InputText from "../../shared/components/InputText";
import CrudCardHeader from "../../shared/components/CrudCardHeader";
import FilterContainer from "../../shared/components/FilterContainer";
import InputSelect from "../../shared/components/InputSelect";
import TableContainer from "../../shared/components/TableContainer";
import TableDataUser from "../../shared/components/TableDataUser";

import { ParticipantImg, ParticipantsImgsContainer, style_EditLink, style_Td, style_TdAction } from "./styles";

const frontsPerpage = 6
export function Fronts() {
  const [filterChecked, setFilterChecked] = useState(false);
  const inputFront = useRef<HTMLInputElement>(null!)
  const [managerValue, setManagerValue] = useState('')
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [numberOfPages, setNumberOfPages] = useState<number>(
    Math.ceil(FRONTS.length / frontsPerpage)
  );
  const [frontsList, setFrontsList] = useState<TFront[]>(FRONTS);
  const [visibleFronts, setVisibleFronts] = useState<TFront[]>(
    frontsList.slice(0, frontsPerpage)
  );

  const updateList = (list: TFront[]) => {
    setFrontsList(list);
    setNumberOfPages(Math.ceil(list.length / frontsPerpage));
    setVisibleFronts(list.slice(0, frontsPerpage));
  };

  const handleInputChange = () => {
    if (!filterChecked) return
    const nameValue = inputFront.current.value
    const newList = FRONTS.filter(front => {
      let show = true
      if (nameValue) {
        show = front.name.toLowerCase().includes(nameValue.toLowerCase())
      }
      if (managerValue) {
        show = show && (USERS[front.manager].name === managerValue)
      }
      return show;
    });
    updateList(newList);
  };

  const handlePageClick = async (pageNumber: number) => {
    // when connect to api change the logic to get admins from backend
    if (pageNumber < 1 || pageNumber > numberOfPages) return;
    setCurrentPage(pageNumber);
    const offset = (pageNumber - 1) * frontsPerpage;
    setVisibleFronts(frontsList.slice(offset, offset + frontsPerpage));
  };

  useEffect(() => {
    if (!filterChecked) updateList(FRONTS)
    handleInputChange()
  }, [filterChecked, managerValue])
  return (
    <div className="min-h-full">
      <Header />
      <main className="-mt-24">
        <div className="max-w-7xl mx-auto pb-24 px-4 sm:px-6 lg:px-8">
          <div className="bg-white rounded-lg shadow px-5 py-8 sm:px-6">

            <CrudCardHeader createNewHref="/frontsdetail" title="Frentes" />
            <FilterContainer
              filterChecked={filterChecked}
              setFilterChecked={setFilterChecked}>
              <InputText
                label="Frente"
                placeholder="Nome da Frente"
                handleInputChange={handleInputChange}
                ref={inputFront}
              />
              <InputSelect
                data={MANAGERS_SELECT}
                filterValue={managerValue}
                setFilterValue={setManagerValue}
                title={'Gerente da Gente'}
              />
            </FilterContainer>

            <TableContainer
              headers={['Nome da Frente', 'Gerente de Gente', 'Participantes']}
              SrOnlyHeaders={['Editar']}
            >
              {visibleFronts.map(front => (
                <tr key={front.id}>
                  <td className={`${style_Td} font-medium text-gray-900`} >
                    {front.name}
                  </td>
                  <td className={style_Td} >
                    <TableDataUser user={USERS[front.manager]} />
                  </td>

                  <td className={style_Td}>
                    <ParticipantsImgsContainer>
                      {front.members.map((member, index) => (
                        <ParticipantImg
                          key={`${member} ${index}`}
                          src={`${USERS[member].image}`}
                        />
                      ))}
                    </ParticipantsImgsContainer>
                  </td>

                  <td className={`${style_Td} ${style_TdAction}`}>
                    <Link
                      to="/frontsdetail"
                      state={{ front }}
                      className={style_EditLink}
                    >
                      Detalhes
                    </Link>
                  </td>
                </tr>
              ))}
            </TableContainer>
            <Pagination
              current={currentPage}
              numberOfPages={numberOfPages}
              onClickPage={handlePageClick}
            />
          </div>
        </div>
      </main>
    </div>
  );
}
