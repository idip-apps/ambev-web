import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";

import { Header } from "../../shared/components/Header";
import { Pagination } from "../../shared/components/Pagination";
import InputSelect from "../../shared/components/InputSelect";
import InputText from "../../shared/components/InputText";
import CrudCardHeader from "../../shared/components/CrudCardHeader";
import FilterContainer from "../../shared/components/FilterContainer";
import TableContainer from "../../shared/components/TableContainer";
import TableDataUser from "../../shared/components/TableDataUser";

import { style_EditLink, style_Td, style_TdAction } from "./styles";

import { CITIES_SELECT, STATES_SELECT, TUnity, UNITIES } from "../../shared/MOCK/unities2";
import { MANAGERS_SELECT, USERS } from "../../shared/MOCK/users2";

const unitiesPerPage = 6;

export function Units() {
  const [filterChecked, setFilterChecked] = useState(false);
  const inputUnity = useRef<HTMLInputElement>(null!)
  const [stateValue, setStateValue] = useState('')
  const [cityValue, setCityValue] = useState('')
  const [managerValue, setManagerValue] = useState('')
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [numberOfPages, setNumberOfPages] = useState<number>(
    Math.ceil(UNITIES.length / unitiesPerPage)
  );
  const [unitiesList, setUnitysList] = useState<TUnity[]>(UNITIES);
  const [visibleUnities, setVisibleUnities] = useState(
    unitiesList.slice(0, unitiesPerPage)
  );

  const updateList = (list: TUnity[]) => {
    setUnitysList(list);
    setNumberOfPages(Math.ceil(list.length / unitiesPerPage));
    setVisibleUnities(list.slice(0, unitiesPerPage));
  };

  const handleInputChange = () => {
    if (!filterChecked) return
    const nameValue = inputUnity.current.value
    const newList = UNITIES.filter(unity => {
      let show = true
      if (nameValue) {
        show = unity.unity.toLowerCase().includes(nameValue.toLowerCase());
      }
      if (cityValue) {
        show = show && unity.city === cityValue;
      }
      if (stateValue) {
        show = show && unity.state === stateValue;
      }
      if (managerValue) {
        show = show && (USERS[unity.manager].name === managerValue)
      }
      return show;
    });
    updateList(newList);
  };

  const handlePageClick = async (pageNumber: number) => {
    // when connect to api change the logic to get admins from backend
    if (pageNumber < 1 || pageNumber > numberOfPages) return;
    setCurrentPage(pageNumber);
    const offset = (pageNumber - 1) * unitiesPerPage;
    setVisibleUnities(unitiesList.slice(offset, offset + unitiesPerPage));
  };

  useEffect(() => {
    if (!filterChecked) updateList(UNITIES);
    handleInputChange();
  }, [filterChecked, cityValue, stateValue, managerValue]);

  return (
    <div className="min-h-full">
      {/* Header */}
      <Header />
      {/*  */}
      <main className="-mt-24">
        <div className="max-w-7xl mx-auto pb-24 px-4 sm:px-6 lg:px-8">
          <div className="bg-white  xl:h-[1000px] h-[1100px]  rounded-lg shadow px-5 py-8 sm:px-6">
            <CrudCardHeader createNewHref="/unitsdetail" title="Unidades" />
            <FilterContainer
              filterChecked={filterChecked}
              setFilterChecked={setFilterChecked}
            >
              <InputText
                label="Unidade"
                placeholder="Nome da Unidade"
                ref={inputUnity}
              />
              <InputSelect
                filterValue={stateValue}
                setFilterValue={setStateValue}
                data={STATES_SELECT}
                title={'Estado'}
              />
              <InputSelect
                filterValue={cityValue}
                setFilterValue={setCityValue}
                data={CITIES_SELECT}
                title={'Cidade'}
              />
              <InputSelect
                filterValue={managerValue}
                setFilterValue={setManagerValue}
                data={MANAGERS_SELECT}
                title={'Gerente de Gente'}
              />
            </FilterContainer>
            <TableContainer
              headers={['Nome da Unidade', 'Estado', 'Cidade', 'Gerente de Gente']}
              SrOnlyHeaders={['Editar']}>
              {visibleUnities.map((unity, index) => (
                <tr key={unity.id} >
                  <td className={`${style_Td} font-medium text-gray-900`}>
                    {unity.unity}
                  </td>
                  <td className={`${style_Td} text-gray-500`}>
                    {unity.state}
                  </td>
                  <td className={`${style_Td} text-gray-500`}>
                    {unity.city}
                  </td>
                  <td className={`${style_Td} text-gray-500`}>
                    <TableDataUser user={USERS[unity.manager]} />
                  </td>
                  <td className={`${style_Td} ${style_TdAction}`} >
                    <Link
                      to="/unitsdetail"
                      state={{ unity }}
                      className={style_EditLink}
                    >
                      Detalhes
                    </Link>
                  </td>
                </tr>
              ))}
            </TableContainer>
            <Pagination
              current={currentPage}
              numberOfPages={numberOfPages}
              onClickPage={handlePageClick}
            />
          </div>
        </div >
      </main >
    </div >
  );
}
