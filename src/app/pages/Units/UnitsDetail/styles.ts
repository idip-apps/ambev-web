import tw from "tailwind-styled-components";

export const Subtitle = tw.p` 
  text-xl 
  text-gray-600 
  font-semibold 
  mb-1
`
export const Text = tw.p` 
  text-gray-400 
  font-normal 
  text-base
`
export const TextBold = tw.span` 
  font-semibold 
  text-gray-700
`
export const InputsContainer = tw.div` 
  flex 
  items-center 
  flex-col 
  justify-center 
  my-10
`