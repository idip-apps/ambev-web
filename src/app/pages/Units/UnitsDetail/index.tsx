import { Header } from "../../../shared/components/Header";
import { useState } from "react";
import { useLocation } from "react-router-dom";
import { CITIES_SELECT, STATES_SELECT, TUnity } from "../../../shared/MOCK/unities2";
import DetailsCardHeader from "../../../shared/components/DetailsCardHeader";
import { MANAGERS_SELECT, USERS } from "../../../shared/MOCK/users2";
import InputSelect from "../../../shared/components/InputSelect";
import InputText from "../../../shared/components/InputText";
import LineSeparator from "../../../shared/components/LineSeparator";
import CreateSaveButton from "../../../shared/components/CreateSaveButton";
import { InputsContainer, Subtitle, Text, TextBold } from "./styles";

interface IState {
  unity: TUnity
}
export function UnitsDetail() {
  const location = useLocation()
  const state = location.state as IState
  const unity = state?.unity

  const [stateValue, setStateValue] = useState(unity?.state)
  const [cityValue, setCityValue] = useState(unity?.city)
  const [managerValue, setManagerValue] = useState(USERS[unity?.manager]?.name)

  return (
    <div className="min-h-full">
      {/* Header */}
      <Header />
      {/*  */}

      <main className="-mt-24">
        <div className="max-w-7xl mx-auto pb-24 px-4 sm:px-6 lg:px-8">
          <div className="bg-white h-full rounded-lg shadow px-5 py-8 sm:px-6">

            <DetailsCardHeader
              createNew={!unity}
              texts={['Criar Unidade', 'Detalhes da Unidade']}
            />
            <div>
              <Subtitle>
                Informações básicas da Unidade
              </Subtitle>
              <Text>
                Insira as informações da{" "}
                <TextBold>Unidade</TextBold>
                {" "}e selecione seu responsável como{" "}
                <TextBold>Gerente de Gente</TextBold>
                .
              </Text>
              <LineSeparator />
            </div>

            <InputsContainer>
              <InputText
                label="Nome da Unidade"
                defaultValue={unity?.unity}
              />
              <InputSelect
                data={STATES_SELECT}
                filterValue={stateValue}
                setFilterValue={setStateValue}
                title='Estado'
              />
              <InputSelect
                data={CITIES_SELECT}
                filterValue={cityValue}
                setFilterValue={setCityValue}
                title='Cidade'
              />
              <InputSelect
                data={MANAGERS_SELECT}
                filterValue={managerValue}
                setFilterValue={setManagerValue}
                title='Gerente de Gente'
              />

            </InputsContainer>
            <LineSeparator />
            <CreateSaveButton type='Unidade' create={!unity} />

          </div>
        </div>
      </main>
    </div>
  );
}
