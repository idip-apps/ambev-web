/* eslint-disable jsx-a11y/anchor-is-valid */
import ambevLogoImg from "../../../assets/ambev-logo.svg";
import logoComiteImg from "../../../assets/logo-comite.png";
import { HiEye, HiEyeOff } from "react-icons/hi";
import { useState } from "react";

export function NewPassword() {
  const [show, setShow] = useState(false);

  const handleClick = (e: any) => {
    e.preventDefault();
    setShow(!show);
  };

  return (
    <>
      <div className="bg-background-image bg">
        <div className="py-8 flex items-center md:h-screen">
          <div className="flex md:px-10 px-2 flex-col flex-wrap max-w-[67rem] mx-auto justify-center">
            <div
              className="md:mb-32 pt-5 mb-5 flex items-end justify-end
             flex-col-reverse md:flex-row relative w-full"
            >
              <div className="w-full lg:w-1/2">
                <div
                  className="bg-slate-50 shadow-[rgb(0,0,0,.12)] max-w-sm
               p-3 xl:p-5 rounded-lg min-w-fit"
                >
                  <div className="w-full px-3 flex items-center justify-center">
                    <a href="#">
                      <img
                        src={logoComiteImg}
                        alt="Logo Comitê de Gente"
                        width="270"
                      />
                    </a>
                  </div>
                  <div className="text-center px-3 mb-5 max-w-fit">
                    <h3 className="text-2xl font-bold mb-2">
                      Mariana Oliveira
                    </h3>
                    <span className="text-gray-500">
                      Você solicitou uma alteração de senha para
                      mariana.oliveira@ambev.com.br
                    </span>
                  </div>
                  <div>
                    <div className="flex flex-wrap">
                      <div className="w-full relative">
                        <input
                          name="email"
                          placeholder="Nova senha"
                          type={show ? "text" : "password"}
                          className="my-3 w-full p-1 border border-gray-300 rounded-md mt-1 h-12 shadow-sm text-sm"
                        />
                        <button
                          type="button"
                          className="absolute right-3 top-[18px] text-gray-400"
                        >
                          <div className="">
                            {show ? (
                              <HiEye size={20} onClick={handleClick} />
                            ) : (
                              <HiEyeOff size={20} onClick={handleClick} />
                            )}
                          </div>
                        </button>
                      </div>
                    </div>
                    <div className="flex flex-wrap">
                      <div className="w-full relative">
                        <input
                          name="password"
                          placeholder="Confirmar nova senha"
                          type={show ? "text" : "password"}
                          className="w-full p-1 border border-gray-300 rounded mt-1 h-12"
                        />
                        <button
                          type="button"
                          className="absolute right-3 top-[18px] text-gray-400 "
                        >
                          <div>
                            {show ? (
                              <HiEye size={20} onClick={handleClick} />
                            ) : (
                              <HiEyeOff size={20} onClick={handleClick} />
                            )}
                          </div>
                        </button>
                      </div>
                    </div>
                    <div className="mt-4 text-center">
                      <button
                        className="h-12 my-6 w-full py-2 px-4 bg-blue-600
            hover:bg-blue-700 rounded-md text-white text-sm"
                      >
                        Solicitar nova senha
                      </button>
                    </div>
                    <div className="text-center text-gray-400 p-3">
                      <a
                        href="/"
                        className="font-medium text-sm text-blue-500 underline hover:no-underline ml-2"
                      >
                        Não é você? Clique aqui.
                      </a>
                    </div>
                  </div>
                </div>
                <a href="#" className="mt-10 flex md:hidden justify-center">
                  <img src={ambevLogoImg} alt="logo ambev" width="100" />
                </a>
              </div>

              <div
                className="lg:pl-28 lg:w-1/2 ml-3 w-full h-full flex justify-end 
            flex-col lg:flex-col"
              >
                <div className="flex flex-col">
                  <h1 className="text-white font-extrabold text-3xl block mb-6">
                    Olá!
                  </h1>

                  <p className="text-white text-lg font-normal block mb-2">
                    Este é o Comitê de Melhores Práticas.
                  </p>
                  <p className="text-white text-lg font-normal mb-5 md:mb-0 pr-16 md:pr-0">
                    Aqui você poderá acompanhar o desempenho da sua unidade,
                    compartilhar suas conquistas e interagir com seus colegas.
                  </p>
                </div>
                <a href="#" className="mt-10 lg:mt-16 hidden md:flex">
                  <img src={ambevLogoImg} alt="logo ambev" width="100" />
                </a>
              </div>
            </div>
            <div
              className="flex sm:flex-row flex-col  lg:flex-row justify-between
          lg:justify-between text-white border-t pt-1 border-white"
            >
              <span className="font-medium">ajuda.mp@ambev.com.br</span>
              <span className="font-medium">+55 11 9999-9999</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
