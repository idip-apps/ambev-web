import React from 'react';
import { IoIosArrowDown } from 'react-icons/io';
import { BsArrowUpShort } from 'react-icons/bs';

import { BadgeCheckIcon, UsersIcon } from '@heroicons/react/outline';

import { HiOutlineNewspaper } from 'react-icons/hi';
import { GoGraph } from 'react-icons/go';
import graphImg from '../../../assets/graph.png';

import { Header } from '../../shared/components/Header';
import { user } from '../../shared/MOCK/dashboard';

const stats = [
  { label: ' Atividades', value: '12 de 24' },
  { label: ' Melhores Práticas', value: '14 de 20' },
  { label: 'Vantagens', value: '2 de 10' },
];
const actions = [
  {
    icon: BadgeCheckIcon,
    name: 'Melhores Práticas',
    href: '#',
    description:
      'Acompanhe aqui o progresso das Melhores Práticas realizado por sua unidade.',
    iconForeground: 'text-teal-700',
    iconBackground: 'bg-teal-50',
  },
  {
    icon: HiOutlineNewspaper,
    name: 'Novidades',
    href: '#',
    description:
      'Veja oque as unidades estão falando sobre seus professores nas Melhores Práticas.',
    iconForeground: 'text-purple-700',
    iconBackground: 'bg-purple-50',
  },
  {
    icon: UsersIcon,
    name: 'Usuários',
    href: '#',
    description:
      'Visualize, cadastre ou edite as informações dos usuários do sistema.',
    iconForeground: 'text-sky-700',
    iconBackground: 'bg-sky-50',
  },
  {
    icon: GoGraph,
    name: 'Relatórios',
    href: '#',
    description:
      'Relatórios do desempenho das Melhores Práticas e Pessoas do Comitê de Melhores Práticas.',
    iconForeground: 'text-yellow-700',
    iconBackground: 'bg-yellow-50',
  },
];
const recentPosts = [
  {
    id: 1,
    postTitle: 'Concluímos a Sala de Conferências!',
    href: '#',
    coment:
      'Cum qui rem deleniti. Suscipit in dolor veritatis sequi aut. Vero ut earum quis deleniti.',
  },
  {
    id: 2,
    postTitle: 'Título do Post',
    href: '#',
    coment:
      'Alias inventore ut autem optio voluptas et repellendus. Facere totam quaerat quam quo laudantium.',
  },
  {
    id: 3,
    postTitle: 'Office closed on July 2nd',
    href: '#',
    coment:
      'Tenetur libero voluptatem rerum occaecati qui est molestiae exercitationem. Voluptate quisquam.',
  },
];

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ');
}

export const Dashboard = () => {
  return (
    <div className="min-h-full">
      {/* Header */}
      <Header />
      {/*  */}

      <main className="-mt-24 pb-8">
        <div className="mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
          <h1 className="sr-only">Profile</h1>
          {/* Main 3 column grid */}
          <div className="grid grid-cols-1 items-start gap-4 lg:grid-cols-3 lg:gap-8 ">
            {/* Left column */}
            <div className="grid grid-cols-1 gap-6 lg:col-span-2">
              {/* Welcome panel */}
              {/* <ProfileOverView /> */}
              <section aria-labelledby="profile-overview-title">
                <div className="overflow-hidden rounded-lg bg-white shadow">
                  <h2 className="sr-only" id="profile-overview-title">
                    Profile Overview
                  </h2>
                  <div className="bg-white p-6">
                    <div className="sm:flex sm:items-center sm:justify-between">
                      <div className="sm:flex sm:space-x-5">
                        <div className="flex-shrink-0">
                          <img
                            className="mx-auto h-20 w-20 rounded-full"
                            src={user.imageUrl}
                            alt=""
                          />
                        </div>
                        <div className="mt-4 text-center sm:mt-0 sm:pt-1 sm:text-left">
                          <p className="text-sm font-medium text-gray-600">
                            {user.role}
                          </p>
                          <p className="text-xl font-bold text-gray-900 sm:text-2xl">
                            {user.name}
                          </p>
                          <p className="text-sm font-medium text-gray-600">
                            {user.unity}
                          </p>
                        </div>
                      </div>
                      <div className="mt-5 flex justify-center sm:mt-0">
                        <a
                          href="/"
                          className="flex items-center justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50"
                        >
                          Ver Perfil
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-1 divide-y divide-gray-200 border-t border-gray-200 bg-gray-50 sm:grid-cols-3 sm:divide-y-0 sm:divide-x">
                    {stats.map((stat) => (
                      <div
                        key={stat.label}
                        className="px-6 py-5 text-center text-sm font-medium"
                      >
                        <span className="text-gray-900">{stat.value}</span>{' '}
                        <span className="text-gray-600">{stat.label}</span>
                      </div>
                    ))}
                  </div>
                </div>
              </section>
              {/* Actions panel */}
              <section aria-labelledby="quick-links-title">
                <div className="divide-y divide-gray-200 overflow-hidden rounded-lg bg-gray-200 shadow sm:grid sm:grid-cols-2 sm:gap-px sm:divide-y-0">
                  <h2 className="sr-only" id="quick-links-title">
                    Quick links
                  </h2>
                  {actions.map((action, actionIdx) => (
                    <div
                      key={action.name}
                      className={classNames(
                        actionIdx === 0
                          ? 'rounded-tl-lg rounded-tr-lg sm:rounded-tr-none'
                          : '',
                        actionIdx === 1 ? 'sm:rounded-tr-lg' : '',
                        actionIdx === actions.length - 2
                          ? 'sm:rounded-bl-lg'
                          : '',
                        actionIdx === actions.length - 1
                          ? 'rounded-bl-lg rounded-br-lg sm:rounded-bl-none'
                          : '',
                        'group relative bg-white p-6 focus-within:ring-2 focus-within:ring-inset focus-within:ring-cyan-500'
                      )}
                    >
                      <div>
                        <span
                          className={classNames(
                            action.iconBackground,
                            action.iconForeground,
                            'inline-flex rounded-lg p-3 ring-4 ring-white'
                          )}
                        >
                          <action.icon className="h-6 w-6" aria-hidden="true" />
                        </span>
                      </div>
                      <div className="mt-8">
                        <h3 className="text-lg font-medium">
                          <a href={action.href} className="focus:outline-none">
                            {/* Extend touch target to entire panel */}
                            <span
                              className="absolute inset-0"
                              aria-hidden="true"
                            />
                            {action.name}
                          </a>
                        </h3>
                        <p className="mt-2 text-sm text-gray-500">
                          {action.description}
                        </p>
                      </div>
                      <span
                        className="pointer-events-none absolute top-6 right-6 text-gray-300 group-hover:text-gray-400"
                        aria-hidden="true"
                      >
                        <svg
                          className="h-6 w-6"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="currentColor"
                          viewBox="0 0 24 24"
                        >
                          <path d="M20 4h1a1 1 0 00-1-1v1zm-1 12a1 1 0 102 0h-2zM8 3a1 1 0 000 2V3zM3.293 19.293a1 1 0 101.414 1.414l-1.414-1.414zM19 4v12h2V4h-2zm1-1H8v2h12V3zm-.707.293l-16 16 1.414 1.414 16-16-1.414-1.414z" />
                        </svg>
                      </span>
                    </div>
                  ))}
                </div>
              </section>
              {/* Estatísticas Por unidade */}
              <div className="max-w-[935px] bg-white rounded-lg shadow-md">
                <div className="p-6 bg-white flex justify-between items-center rounded-lg">
                  <div className="flex flex-col w-full">
                    <h2 className="text-2xl font-bold text-gray-800">
                      Estatísticas por Unidade
                    </h2>
                  </div>
                  <button
                    className="border border-gray-400 rounded-md h-12 w-48 font-semibold 
          shadow-md flex items-center justify-between p-3"
                  >
                    <p>Rio de Janeiro</p>
                    <IoIosArrowDown />
                  </button>
                </div>
                <div
                  className="lg:flex lg:flex-row flex-col justify-evenly items-center 
                    font-medium text-gray-600 border bg-white rounded-lg"
                >
                  {/* Graph 1 */}

                  <div className="flex w-full items-center justify-center">
                    <img
                      src={graphImg}
                      alt="Gráfico circular"
                      className="mr-10"
                    />
                    <div className="flex flex-col w-1/2">
                      <p className="text-xl mb-4">Atividades realizadas</p>
                      <div className="flex justify-between ">
                        <p className="text-xl font-bold text-black">
                          125{' '}
                          <span className="text-sm font-normal">de 254</span>
                        </p>
                        <div
                          className="bg-green-200 flex justify-center
                          items-center rounded-lg px-1 text-green-700"
                        >
                          <BsArrowUpShort
                            className="text-green-600"
                            size={25}
                          />
                          <p>12%</p>{' '}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="h-32 border hidden lg:block" />
                  <div className="w-full border lg:hidden block" />

                  {/* Graph 2 */}
                  <div className="flex w-full items-center justify-center">
                    <img
                      src={graphImg}
                      alt="Gráfico circular"
                      className="mr-10"
                    />
                    <div className="flex flex-col w-1/2">
                      <p className="text-xl mb-4">Atividades realizadas</p>
                      <div className="flex justify-between ">
                        <p className="text-xl font-bold text-black">
                          125{' '}
                          <span className="text-sm font-normal">de 254</span>
                        </p>
                        <div
                          className="bg-green-200 flex justify-center
                          items-center rounded-lg px-1 text-green-700"
                        >
                          <BsArrowUpShort
                            className="text-green-600"
                            size={25}
                          />
                          <p>12%</p>{' '}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Sumário das Frentes */}
              <div className="max-w-[935px] bg-white rounded-lg shadow-md">
                <div className="px-6 pt-6 mb-8  flex justify-between items-center rounded-lg">
                  <div className="flex flex-col w-full">
                    <h2 className="text-2xl font-bold text-gray-800">
                      Sumário das Frentes
                    </h2>
                  </div>
                  <button
                    type="button"
                    className="border border-gray-400 rounded-md h-12 w-32 font-semibold 
          shadow-md flex items-center justify-between p-3"
                  >
                    <p>Filtro</p>
                    <IoIosArrowDown />
                  </button>
                </div>
                <div className="px-6 pb-5">
                  <div className="flex justify-between p-4 bg-gray-200 rounded-lg shadow-sm font-bold mb-3">
                    <span>Frente</span>
                    <span>Atividades</span>
                    <span>Status</span>
                  </div>
                  <div
                    className="flex justify-between p-4 rounded-lg shadow-sm border-2 border-gray-300 mb-3
                    "
                  >
                    <span>Eventos & Família</span>
                    <span>26 de 26</span>
                    <span
                      className="bg-green-200 flex justify-center items-center 
                      rounded-lg px-3 text-green-700 font-semibold"
                    >
                      Completo
                    </span>
                  </div>
                  <div
                    className="flex justify-between p-4 rounded-lg shadow-sm border-2 border-gray-300 mb-3
                    "
                  >
                    <span>Eventos & Família</span>
                    <span>26 de 26</span>
                    <span
                      className="bg-green-200 flex justify-center items-center 
                      rounded-lg px-3 text-green-700 font-semibold"
                    >
                      Completo
                    </span>
                  </div>
                  <div
                    className="flex justify-between p-4 rounded-lg shadow-sm border-2 border-gray-300
                    "
                  >
                    <span>Eventos & Família</span>
                    <span>26 de 26</span>
                    <span
                      className="bg-green-200 flex justify-center items-center 
                      rounded-lg px-3 text-green-700 font-semibold"
                    >
                      Completo
                    </span>
                  </div>
                </div>
              </div>
            </div>

            {/*  */}

            {/* Right column */}

            {/* End Right column */}
          </div>
        </div>
      </main>
      <footer />
    </div>
  );
};

// Right Column
//  <div className="grid grid-cols-1 gap-5">
//    {/* Postagens Recentes */}
//    <section aria-labelledby="announcements-title">
//      <div className="overflow-hidden rounded-lg bg-white shadow">
//        <div className="p-6">
//          <div className="flex justify-between">
//            <h2
//              className="text-base font-bold text-gray-900"
//              id="announcements-title"
//            >
//              Postagens Recentes
//            </h2>
//          </div>
//          <div className="mt-6 flow-root">
//            <ul role="list" className="-my-5 divide-y divide-gray-200">
//              {recentPosts.map((recentPost) => (
//                <li key={recentPost.id} className="py-5">
//                  <div className="relative focus-within:ring-2 focus-within:ring-cyan-500">
//                    <h3 className="text-sm font-semibold text-gray-800">
//                      <div className="flex justify-between">
//                        <PostTime />
//                        <span
//                          className="pointer-events-none top-6
//                         right-6 text-gray-300 group-hover:text-gray-400"
//                          aria-hidden="true"
//                        >
//                          <svg
//                            className="h-6 w-6"
//                            xmlns="http://www.w3.org/2000/svg"
//                            fill="currentColor"
//                            viewBox="0 0 24 24"
//                          >
//                            <path d="M20 4h1a1 1 0 00-1-1v1zm-1 12a1 1 0 102 0h-2zM8 3a1 1 0 000 2V3zM3.293 19.293a1 1 0 101.414 1.414l-1.414-1.414zM19 4v12h2V4h-2zm1-1H8v2h12V3zm-.707.293l-16 16 1.414 1.414 16-16-1.414-1.414z" />
//                          </svg>
//                        </span>
//                      </div>
//                      <a
//                        href={recentPost.href}
//                        className="hover:underline focus:outline-none"
//                      >
//                        {/* Extend touch target to entire panel */}
//                        <span className="absolute inset-0" aria-hidden="true" />
//                        {recentPost.postTitle}
//                      </a>
//                    </h3>
//                    <p className="line-clamp-2 mt-1 text-sm text-gray-600">
//                      {recentPost.coment}
//                    </p>
//                  </div>
//                  <div className="flex items-center mt-3">
//                    <AiFillLike size={15} />
//                    <span className="text-base ml-1 mr-6">156</span>
//                    <AiFillMessage size={15} />
//                    <span className="text-base ml-1">156</span>
//                  </div>
//                </li>
//              ))}
//            </ul>
//          </div>
//          <div className="mt-6">
//            <a
//              href="#"
//              className="text-white flex w-full items-center justify-center
//                            rounded-md border border-gray-300 bg-indigo-600 px-4
//                            py-2 text-sm font-medium shadow-sm hover:bg-gray-50"
//            >
//              Ver todas
//            </a>
//          </div>
//        </div>
//      </div>
//    </section>

//    {/* Need Help? */}
//    <section aria-labelledby="recent-hires-title">
//      <div className="overflow-hidden rounded-lg bg-white shadow">
//        <div className="p-6">
//          <div className="rounded-lg">
//            <img src={helpfulImg} alt="Precisa de ajuda?" width="320px" />
//            <p className="text-xl font-bold text-gray-800 mt-5">
//              Precisa de ajuda?
//            </p>
//            <p className="text-gray-600 mb-5">
//              Você está tendo alguma dificuldade ou precisa de ajuda para
//              utilizar o Comitê de Gente?
//            </p>
//          </div>

//          <div className="mt-6">
//            <a
//              href="#"
//              className="flex w-full items-center justify-center rounded-md
//                           border border-gray-300 bg-indigo-600 px-4 py-2 text-sm font-medium
//                            text-white shadow-sm hover:bg-indigo-500"
//            >
//              Clique aqui
//            </a>
//          </div>
//        </div>
//      </div>
//    </section>
//  </div>;
