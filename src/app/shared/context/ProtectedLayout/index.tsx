import { useAuth } from "../../hooks/useAuth";
import { Navigate } from "react-router-dom";

export const ProtectedLayout = ({ children }: { children: JSX.Element }) => {
  const auth = useAuth();

  return (
    <>
      {auth.email ?
        children :
        <Navigate to="/" state={{ status: "error", message: "Please Login" }} />}
    </>
  )
}
