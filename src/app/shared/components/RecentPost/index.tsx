const posts = [
  {
    date: "Postado em 21/10/21",
    author: {
      name: "Tatiana Maia Gomes",
      href: "#",
      imageUrl:
        "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
      dateTime: "às 17:45",
    },
  },
];

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

export function PostTime() {
  return (
    <div className="mb-4">
      {posts.map((post) => (
        <div key={post.author.name}>
          <div className="flex items-center">
            <div className="flex-shrink-0">
              <a href={post.author.href}>
                <span className="sr-only">{post.author.name}</span>
                <img
                  className="h-10 w-10 rounded-full"
                  src={post.author.imageUrl}
                  alt=""
                />
              </a>
            </div>
            <div className="ml-3">
              <p className="text-sm font-medium text-gray-900">
                <a href={post.author.href}>{post.author.name}</a>
              </p>
              <div className="flex space-x-1 text-sm text-gray-500">
                <time>{post.date}</time>

                <span>{post.author.dateTime}</span>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
