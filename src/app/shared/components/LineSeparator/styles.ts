import tw from "tailwind-styled-components";

export const Container = tw.div`
  relative 
  mt-5
`
export const LineContainer = tw.div`
  absolute 
  inset-0 
  flex 
  items-center
`
export const Line = tw.div`
  w-full 
  border-t 
  border-gray-300
`
export const BottomSeparator = tw.div`
  relative 
  flex 
  items-center 
  justify-between
`