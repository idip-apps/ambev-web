import React from 'react';
import { BottomSeparator, Container, Line, LineContainer } from './styles';

const LineSeparator: React.FC = () => {
  return (
    <Container>
      <LineContainer aria-hidden="true">
        <Line />
      </LineContainer>
      <BottomSeparator />
    </Container>
  )
}

export default LineSeparator;