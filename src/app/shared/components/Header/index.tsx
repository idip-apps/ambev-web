import { Menu, Popover, Transition } from "@headlessui/react";
import {
  BellIcon,
  MenuIcon,
  SearchIcon,
  XIcon,
} from "@heroicons/react/outline";

import { Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Outlet } from "react-router-dom";
import { navigation, user, userNavigation } from "../../MOCK/dashboard";
import { Adminstration } from "./adminstration";

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}


export const admNavigation = [
  { page: "Unidades", href: "/units" },
  { page: "Usuários", href: "/users" },
  { page: "Frentes", href: "/fronts" },
  { page: "Cadastro de MPs", href: "#" },
  { page: "Atividades", href: "#" },
  { page: "Relatórios", href: "#" },
];

export function Header() {
  return (
    <>
      <Popover
        as="header"
        className="bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 pb-24 min-w-[296px]"
      >
        {({ open }) => (
          <>
            <div className="mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
              <div className="relative flex flex-wrap items-center justify-center lg:justify-between">
                {/* Header */}
                {/* Logo */}
                <div className="absolute left-0 flex-shrink-0 py-5 lg:static">
                  <a href="#">
                    <span className="sr-only">Workflow</span>
                    <img
                      src="https://ambev-mp.abcdev.net/_nuxt/img/logo.ce567b8.svg"
                      alt="Logo Ambev"
                      className="h-8 w-auto"
                    />
                  </a>
                </div>

                {/* Right section on desktop */}
                <div className="hidden lg:ml-4 lg:flex lg:items-center lg:py-5 lg:pr-0.5">
                  <button
                    type="button"
                    className="flex-shrink-0 rounded-full p-1 text-gray-300 hover:bg-white hover:bg-opacity-10 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                  >
                    <span className="sr-only">View notifications</span>
                    <BellIcon className="h-6 w-6" aria-hidden="true" />
                  </button>

                  {/* Profile dropdown */}
                  <Menu as="div" className="relative ml-4 flex-shrink-0">
                    <div>
                      <Menu.Button className="flex rounded-full bg-white text-sm ring-2 ring-white ring-opacity-20 focus:outline-none focus:ring-opacity-100">
                        <span className="sr-only">Open user menu</span>
                        <img
                          className="h-8 w-8 rounded-full"
                          src={user.imageUrl}
                          alt=""
                        />
                      </Menu.Button>
                    </div>
                    <Transition
                      as={Fragment}
                      leave="transition ease-in duration-75"
                      leaveFrom="transform opacity-100 scale-100"
                      leaveTo="transform opacity-0 scale-95"
                    >
                      <Menu.Items className="absolute -right-2 z-40 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                        {userNavigation.map((item) => (
                          <Menu.Item key={item.name}>
                            {({ active }) => (
                              <a
                                href={item.href}
                                className={classNames(
                                  active ? "bg-gray-100" : "",
                                  "block px-4 py-2 text-sm text-gray-700"
                                )}
                              >
                                <FormattedMessage id={item.name} />
                              </a>
                            )}
                          </Menu.Item>
                        ))}
                      </Menu.Items>
                    </Transition>
                  </Menu>
                </div>

                {/* Summary */}
                <div className="w-full py-5 lg:border-t lg:border-white lg:border-opacity-20">
                  <div className="lg:grid lg:grid-cols-3 lg:items-center lg:gap-8">
                    {/* Left nav */}
                    <div className="hidden lg:col-span-2 lg:block">
                      <nav className="flex space-x-4">
                        {navigation.map((item) => (
                          <a
                            key={item.name}
                            href={item.href}
                            className={classNames(
                              item.current ? "text-white " : "text-gray-300",
                              "rounded-md bg-white bg-opacity-0 px-3 py-2 text-sm font-medium hover:bg-opacity-10"
                            )}
                            aria-current={item.current ? "page" : undefined}
                          >
                            <FormattedMessage id={item.name} />
                          </a>
                        ))}
                        {/* Adiministração button */}
                        <Menu as="div" className="relative flex-shrink-0">
                          <div>
                            <Menu.Button
                              className="flex rounded-md text-sm focus:outline-none 
                       bg-white bg-opacity-0 font-medium hover:bg-opacity-10"
                            >
                              <span className="sr-only"></span>
                              <span className="block px-3 py-2 text-sm text-gray-300">
                                <FormattedMessage id="HEADER.ADMIN" />
                              </span>
                            </Menu.Button>
                          </div>
                          <Transition
                            as={Fragment}
                            leave="transition ease-in duration-75"
                            leaveFrom="transform opacity-100 scale-100"
                            leaveTo="transform opacity-0 scale-95"
                          >
                            <Menu.Items
                              className="absolute  z-40 mt-2 w-48 origin-top-right
                             rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 
                             focus:outline-none"
                            >
                              {admNavigation.map((item) => (
                                <Menu.Item key={item.page}>
                                  {({ active }) => (
                                    <a
                                      href={item.href}
                                      className={classNames(
                                        active ? "bg-gray-100" : "",
                                        "block px-4 py-2 text-sm text-gray-700"
                                      )}
                                    >
                                      {item.page}
                                    </a>
                                  )}
                                </Menu.Item>
                              ))}
                            </Menu.Items>
                          </Transition>
                        </Menu>
                        {/* End Adiministração button */}
                      </nav>
                    </div>
                    <div className="px-[88px] pr-9 lg:px-0">
                      {/* Search */}
                      <div className="mx-auto w-full max-w-xs lg:max-w-md">
                        <label htmlFor="search" className="sr-only">
                          Search
                        </label>
                        <div className="relative text-white focus-within:text-gray-600">
                          <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                            <SearchIcon className="h-5 w-5" aria-hidden="true" />
                          </div>
                          <input
                            id="search"
                            className="block w-full rounded-md border border-transparent bg-white bg-opacity-20 py-2 pl-10 pr-3 leading-5 text-white placeholder-white focus:border-transparent focus:bg-opacity-100 focus:text-gray-900 focus:placeholder-gray-500 focus:outline-none focus:ring-0 sm:text-sm"
                            placeholder="Pesquisar Novidades"
                            type="search"
                            name="search"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                {/* Menu button */}
                <div className="absolute right-0 flex-shrink-0 lg:hidden">
                  {/* Mobile menu button */}
                  <Popover.Button
                    className="inline-flex items-center 
                    justify-center rounded-md bg-transparent p-2 text-cyan-200
                     hover:bg-white hover:bg-opacity-10 hover:text-white focus:outline-none
                      focus:ring-2 focus:ring-white"
                  >
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <XIcon className="block h-6 w-6" aria-hidden="true" />
                    ) : (
                      <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                    )}
                  </Popover.Button>
                </div>
              </div>
            </div>

            {/* Items Menu Button */}
            <Transition.Root as={Fragment}>
              <div className="lg:hidden">
                <Transition.Child
                  as={Fragment}
                  enter="duration-150 ease-out"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="duration-150 ease-in"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <Popover.Overlay className="fixed inset-0 z-20 bg-black bg-opacity-25" />
                </Transition.Child>

                <Transition.Child
                  as={Fragment}
                  enter="duration-150 ease-out"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="duration-150 ease-in"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Popover.Panel
                    focus
                    className="absolute inset-x-0 top-0 z-30 mx-auto w-full max-w-3xl origin-top transform p-2 transition"
                  >
                    <div className="divide-y divide-gray-200 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
                      <div className="pt-3 pb-2">
                        <div className="flex items-center justify-between px-4">
                          <div>
                            <svg
                              className="h-8 w-auto"
                              viewBox="0 0 88 34"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M6.38596 21.9984C10.9631 19.7847 12.3363 27.1918 9.30835 29.1075C6.5796 30.8272 3.73657 27.7707 4.82802 23.8884C5.17134 22.6709 5.62013 22.3729 6.38596 22.0069V21.9984ZM10.6507 19.0227C8.767 17.056 5.29456 17.1198 2.99717 18.7887C-0.946252 21.6664 -1.0871 29.3972 3.103 32.377C6.01657 34.4459 8.38837 33.4732 10.9762 32.0771L11.0011 33.3222L15.2878 33.2789L15.2531 17.8261L11.0015 17.8599C10.8873 18.0981 10.9399 18.9582 10.8783 19.1029C10.5086 19.8692 11.0894 19.2859 10.6537 19.0221L10.6507 19.0227Z"
                                fill="gray"
                              />
                              <path
                                d="M10.7092 19.4373C10.7291 19.4276 10.749 19.4565 10.7679 19.4517C10.7778 19.4517 10.7828 19.4421 10.7928 19.4373C10.8077 19.4325 10.8176 19.4373 10.8325 19.4373C10.8673 19.4421 10.8722 19.4517 10.8524 19.4916C10.8489 19.4964 10.8126 19.4965 10.8076 19.4965C10.7778 19.4965 10.7978 19.5013 10.7779 19.4772C10.7749 19.4724 10.7679 19.4724 10.7679 19.4724C10.7654 19.4628 10.7778 19.4483 10.7828 19.4434C10.7862 19.4338 10.7868 19.4243 10.7928 19.4195C10.8027 19.4099 10.8498 19.4147 10.8618 19.4195L10.8569 19.4694C10.842 19.4694 10.8419 19.4646 10.822 19.4694C10.8171 19.4697 10.8121 19.4743 10.8072 19.4694C10.7972 19.4646 10.7923 19.4599 10.7873 19.455C10.7774 19.455 10.7675 19.4551 10.7725 19.4599C10.7744 19.4599 10.7768 19.4551 10.7773 19.4599C10.7793 19.4601 10.7793 19.4647 10.7808 19.4647C10.7853 19.4599 10.7858 19.455 10.7908 19.4502C10.7933 19.4502 10.7957 19.4507 10.8007 19.4502C10.8305 19.4502 10.7972 19.4507 10.8255 19.4502C10.827 19.4501 10.8301 19.4503 10.8301 19.4501C10.8326 19.5107 10.8151 19.4789 10.8399 19.4934C10.8419 19.4934 10.8359 19.4934 10.8349 19.4934C10.82 19.4934 10.8101 19.4982 10.7952 19.4934C10.7853 19.4886 10.7803 19.4788 10.7703 19.4788C10.7505 19.474 10.7307 19.503 10.7118 19.4934V19.4429L10.7092 19.4373Z"
                                fill="gray"
                                stroke="gray"
                                stroke-width="0.122158"
                              />
                              <path
                                d="M10.7505 19.3358C10.7555 19.3358 10.7604 19.3407 10.7654 19.3455C10.7679 19.346 10.8053 19.3459 10.8053 19.3459C10.8252 19.346 10.845 19.3459 10.8634 19.3459C10.8832 19.3461 10.8931 19.341 10.8732 19.3796C10.8727 19.3796 10.8434 19.3796 10.8335 19.3796C10.8136 19.3892 10.7988 19.3555 10.7789 19.3555C10.7739 19.3559 10.769 19.3603 10.764 19.3603C10.7591 19.3603 10.754 19.3652 10.744 19.3652C10.7192 19.3459 10.7044 19.3107 10.7342 19.2891C10.7391 19.2843 10.7491 19.2843 10.754 19.2795C10.759 19.2795 10.7689 19.2747 10.7739 19.2792C10.7789 19.284 10.7937 19.3081 10.8086 19.2985C10.8335 19.284 10.8137 19.2696 10.8485 19.2792C10.8932 19.2985 10.8385 19.3389 10.8186 19.3278C10.8137 19.323 10.8087 19.3134 10.8038 19.3086C10.7889 19.299 10.769 19.3279 10.764 19.3327C10.7293 19.3327 10.6944 19.3037 10.7242 19.2691C10.7281 19.2643 10.7342 19.2644 10.7392 19.2595C10.7436 19.2595 10.7739 19.2498 10.7789 19.2498C10.7888 19.2547 10.7889 19.2739 10.8038 19.2691C10.8137 19.2691 10.8137 19.2498 10.8186 19.245C10.8221 19.245 10.8534 19.245 10.8583 19.245C10.9031 19.245 10.9438 19.2961 10.8782 19.3135C10.8683 19.3135 10.8197 19.3182 10.8162 19.3086C10.7914 19.26 10.821 19.2941 10.8121 19.2845C10.8126 19.2749 10.8086 19.2652 10.8141 19.2604C10.8185 19.2556 10.8757 19.2653 10.8837 19.2653L10.8738 19.3149C10.8688 19.3149 10.8291 19.31 10.8241 19.3052C10.8196 19.3004 10.819 19.2908 10.8141 19.2811C10.819 19.2908 10.8102 19.2763 10.8241 19.2619C10.8271 19.2571 10.8589 19.2667 10.8688 19.2619C10.8837 19.2621 10.8638 19.2619 10.8638 19.286C10.8639 19.2908 10.8688 19.2956 10.8738 19.3004C10.8741 19.3009 10.8753 19.3005 10.8753 19.3002C10.8704 19.2954 10.8653 19.2955 10.8603 19.2955C10.8578 19.2953 10.8306 19.2907 10.8306 19.2907C10.8207 19.2859 10.8207 19.2666 10.8107 19.2666C10.8008 19.2666 10.8057 19.2859 10.7957 19.2955C10.7908 19.3003 10.7808 19.2952 10.7759 19.3002C10.7659 19.3002 10.7858 19.2761 10.761 19.2761C10.7605 19.276 10.7615 19.2761 10.7615 19.2761C10.7644 19.2761 10.7665 19.2761 10.7715 19.2761C10.7764 19.281 10.8013 19.3099 10.8112 19.3002C10.8162 19.2954 10.8161 19.2858 10.821 19.2761C10.831 19.2665 10.9041 19.2762 10.8311 19.3195C10.836 19.3195 10.8306 19.3198 10.8276 19.3148C10.8226 19.3051 10.8225 19.2955 10.8176 19.2907C10.8027 19.2762 10.7928 19.31 10.7878 19.3148C10.7854 19.3196 10.7778 19.3195 10.7778 19.3195C10.7768 19.3193 10.7749 19.32 10.7739 19.319C10.7736 19.319 10.7695 19.2949 10.775 19.3142C10.7745 19.3334 10.7773 19.3576 10.8047 19.348C10.8096 19.348 10.8147 19.3383 10.8197 19.3383C10.8247 19.3383 10.8296 19.3383 10.8345 19.3383C10.8445 19.3383 10.8743 19.3383 10.8743 19.3383C10.8941 19.3768 10.8841 19.3721 10.8643 19.3721C10.8444 19.3721 10.8246 19.372 10.8062 19.3721C10.8052 19.3721 10.7665 19.3721 10.7665 19.3725C10.7615 19.3725 10.7566 19.3772 10.7516 19.3821V19.334L10.7505 19.3358Z"
                                fill="gray"
                                stroke="gray"
                                stroke-width="0.122158"
                              />
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M21.7201 33.2878C22.0371 30.6144 20.3029 20.4573 24.5193 21.5811C27.8201 22.4666 24.3697 30.4953 27.16 33.3219L31.6315 33.2367C30.1439 30.4186 30.5136 30.742 30.5048 27.1236C30.5048 25.0121 30.0999 21.5471 32.7494 21.5471C36.9657 21.5471 32.6704 30.5206 36.1647 33.3132L40.6626 33.2699C37.7579 30.29 40.1961 25.2328 38.9109 20.8822C38.427 19.2475 36.9745 17.7917 34.8532 17.5873C31.9308 17.3062 31.2354 18.2429 29.6422 19.52C28.6035 18.7537 28.1194 17.7066 25.8924 17.5788C23.8591 17.4594 22.4743 18.0858 21.5324 19.1245L21.4977 18.0667C21.4778 17.956 21.4465 17.9117 21.3939 17.8352L17.2664 17.8939L17.3722 33.3295L21.7205 33.2959L21.7201 33.2878Z"
                                fill="gray"
                              />
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M47.7128 22.0406C52.3428 19.5971 53.5662 27.6939 50.4238 29.2264C48.0912 30.3588 46.4981 28.5622 46.0316 26.9361C45.5561 25.2929 45.8467 23.0282 47.7128 22.0406ZM45.8783 32.2805C47.9452 33.8756 51.9202 33.9346 53.98 32.0701C58.2843 28.1537 57.4481 21.1467 53.4343 18.5414C50.3711 16.5577 48.1969 17.8264 46.0316 19.0864L45.9967 12.3434L41.49 12.4199L41.6042 33.3301L45.7653 33.3542C45.8801 32.2745 45.7668 33.3637 45.7655 33.3686L45.8783 32.2805Z"
                                fill="gray"
                              />
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M62.5009 23.9565C63.1963 20.2274 67.3159 20.2956 67.844 23.9662L62.5009 23.9565ZM71.6202 28.3412C68.6714 29.3713 63.654 31.3636 62.4921 27.1833L72.3419 27.0047C72.5357 22.603 71.1712 19.24 67.9583 17.98C59.4817 14.6596 54.2708 26.6556 60.5556 31.7554C63.1963 33.9009 68.3016 34.3097 71.0655 32.0448C71.5057 31.1679 71.5938 29.6353 71.6114 28.3412H71.6202Z"
                                fill="gray"
                              />
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M87.3146 17.8436L82.5614 17.9288L79.5071 27.9496L76.5493 17.8351L71.849 17.8448C71.9985 19.1644 76.479 32.1821 77.1479 33.3145L82.0948 33.3385L87.3234 17.8518L87.3146 17.8436Z"
                                fill="gray"
                              />
                              <path
                                d="M67.8471 3.77569C67.8434 3.77566 67.8414 3.77597 67.8406 3.77682C67.7262 3.89528 64.9548 8.03585 64.9766 8.05573C64.9921 8.06993 65.2191 8.14735 65.481 8.22777C65.743 8.30819 65.983 8.3819 66.0145 8.39159C66.0568 8.40463 66.1384 8.30305 66.3274 8.00205C66.4681 7.77809 66.5947 7.58773 66.6087 7.57904C66.6321 7.56459 68.0439 7.99389 68.0717 8.02387C68.0785 8.03104 68.0689 8.25817 68.0531 8.52862C68.0361 8.79908 68.026 9.02336 68.0305 9.02703C68.035 9.0307 68.2925 9.11424 68.603 9.21263C68.9134 9.31101 69.1856 9.39232 69.2078 9.39331C69.2579 9.39557 69.2739 9.0475 69.3482 6.33159C69.3796 5.18322 69.3911 4.26069 69.3741 4.24452C69.3212 4.19397 67.9563 3.77669 67.847 3.7756L67.8471 3.77569ZM68.258 4.99916C68.2662 5.00732 68.2456 5.45602 68.2122 5.99631C68.1789 6.5366 68.1421 6.98869 68.1304 7.00096C68.1024 7.0304 67.1751 6.74045 67.1747 6.70209C67.1747 6.65829 68.2406 4.98179 68.258 4.99916Z"
                                fill="gray"
                              />
                              <path
                                d="M19.9018 8.72693C19.0408 8.61098 18.3245 7.95222 18.0008 6.97867C17.8166 6.42492 17.7782 5.98955 17.8709 5.50525C18.0737 4.44524 19.0251 3.68897 20.343 3.54022C20.8871 3.47882 20.8742 3.47569 20.9544 3.68902C20.9928 3.79088 21.0639 3.97937 21.1125 4.10788L21.2008 4.34154L20.8378 4.38239C19.9661 4.48048 19.4389 4.75224 19.209 5.22201C18.9336 5.78499 19.1556 6.77637 19.6749 7.3021C19.8624 7.49199 19.9225 7.52967 20.1472 7.5979C20.4616 7.69342 20.7361 7.67946 21.1552 7.54665C21.4878 7.44121 21.7952 7.26243 22.0871 7.00455L22.2975 6.81877L22.3209 6.94355C22.3998 7.36396 22.231 7.84947 21.903 8.14585C21.4555 8.55013 20.5473 8.81385 19.9018 8.72693Z"
                                fill="gray"
                              />
                              <path
                                d="M62.9347 7.4954C62.6499 7.42464 62.4082 7.35791 62.3977 7.34711C62.3793 7.32823 63.5191 2.56117 63.5493 2.53098C63.5663 2.51411 64.6629 2.77972 64.6905 2.80739C64.7004 2.81738 64.448 3.90288 64.1294 5.21962C63.7196 6.9131 63.5358 7.6152 63.5013 7.61887C63.4745 7.62172 63.2195 7.56616 62.9347 7.4954Z"
                                fill="gray"
                              />
                              <path
                                d="M23.9118 5.15826C23.5861 3.83687 23.3257 2.74965 23.3332 2.7422C23.3707 2.70465 26.7975 1.88335 26.8191 1.90671C26.8667 1.95792 27.0367 2.76866 27.0059 2.79728C26.989 2.81292 26.4713 2.94822 25.8553 3.09795C25.0389 3.29638 24.7352 3.38378 24.7352 3.4203C24.7352 3.46892 24.949 4.35725 24.966 4.37893C24.9705 4.38486 25.3611 4.29514 25.8338 4.17951C26.3064 4.06388 26.7236 3.96376 26.7607 3.95703C26.82 3.94631 26.8425 4.00068 26.9419 4.39501C27.0044 4.64262 27.0425 4.85322 27.0267 4.863C27.0109 4.8728 26.6051 4.97659 26.1249 5.09367C25.6448 5.21075 25.2411 5.31325 25.2278 5.32145C25.2086 5.33331 25.4236 6.32163 25.4511 6.34772C25.455 6.35156 25.9902 6.22383 26.6402 6.06386C27.2902 5.90389 27.8265 5.77622 27.832 5.78017C27.8373 5.78412 27.8789 5.99547 27.9243 6.24985C27.9989 6.66867 28.0013 6.71388 27.9497 6.72868C27.7768 6.77819 24.5699 7.56077 24.5399 7.56077C24.5201 7.56077 24.2375 6.47964 23.9118 5.15826H23.9118Z"
                                fill="gray"
                              />
                              <path
                                d="M60.085 6.85253C59.9791 6.83376 59.8111 6.78112 59.7115 6.73555C59.32 6.55632 59.1455 6.26494 58.8718 5.3335C58.6675 4.63823 58.5963 4.44864 58.4456 4.19842L58.3248 3.99769H58.5651C59.1827 3.99769 59.5543 3.67413 59.5543 3.13637C59.5543 2.88373 59.434 2.70292 59.1807 2.57505C59.0469 2.50749 58.3815 2.37042 58.3491 2.40373C58.3361 2.417 58.1658 3.29943 57.9703 4.36473C57.7748 5.43002 57.6074 6.30914 57.5982 6.31833C57.5793 6.33718 56.5688 6.15856 56.4818 6.12101C56.4324 6.09965 56.479 5.80501 56.8715 3.67267C57.1168 2.33945 57.3246 1.24268 57.3335 1.23541C57.3679 1.20697 59.4124 1.6024 59.6886 1.6909C60.1676 1.8444 60.5315 2.12407 60.7258 2.48798C60.7967 2.62072 60.8067 2.68977 60.8067 3.04519C60.8067 3.41031 60.7974 3.47029 60.7141 3.64492C60.5125 4.06711 60.1324 4.33964 59.6853 4.38242L59.4661 4.40339L59.5787 4.48366C59.7514 4.60687 59.9942 5.0023 60.2391 5.55899C60.5464 6.25774 60.7184 6.51787 60.9815 6.68187L61.0858 6.74687L60.9469 6.80493C60.7784 6.87534 60.3477 6.89912 60.085 6.85253Z"
                                fill="gray"
                              />
                              <path
                                d="M30.0093 6.27832C29.9913 6.22006 29.1815 1.59812 29.1712 1.49486C29.1624 1.40872 29.1647 1.40815 30.2032 1.22271C31.4086 1.00748 31.828 0.982812 32.2206 1.10408C32.6781 1.24538 32.9642 1.52478 33.1003 1.96318C33.2969 2.59646 33.0539 3.24384 32.5136 3.52615C32.3153 3.62975 32.2724 3.67995 32.3816 3.68054C32.5312 3.68138 32.8628 3.93385 33.3782 4.43968C33.9512 5.00195 34.1787 5.17505 34.4277 5.23811C34.5102 5.25899 34.5776 5.28931 34.5776 5.30548C34.5776 5.34625 34.3071 5.52353 34.1367 5.59441C33.8879 5.69787 33.6849 5.7372 33.3958 5.73794C32.8869 5.73935 32.6858 5.58471 31.9774 4.64843C31.5731 4.11389 31.3846 3.90659 31.1381 3.72507C31.0799 3.68222 31.0515 3.64666 31.075 3.64604C31.2056 3.64263 31.5283 3.47911 31.6882 3.33534C31.9074 3.13832 31.9673 3.00939 31.9663 2.73766C31.9635 2.25117 31.6665 1.98581 31.1766 2.03422C30.7395 2.07741 30.5207 2.13135 30.5207 2.19592C30.5207 2.22913 30.6715 3.10681 30.8558 4.14631C31.0402 5.18581 31.191 6.052 31.191 6.07118C31.191 6.09034 31.0362 6.13359 30.847 6.16727C30.6579 6.20094 30.3948 6.24881 30.2624 6.27364C30.0832 6.30727 30.0186 6.30847 30.0093 6.2783V6.27832Z"
                                fill="gray"
                              />
                              <path
                                d="M52.3555 0.519602C52.3124 0.50789 52.079 1.02544 51.2863 2.89005C50.7286 4.20201 50.2793 5.28247 50.288 5.29109C50.3159 5.31917 51.4222 5.43596 51.445 5.41324C51.4571 5.4011 51.5444 5.18878 51.6389 4.9414C51.7555 4.6362 51.8277 4.49218 51.8637 4.49343C52.0179 4.49876 53.3107 4.65669 53.3261 4.67207C53.336 4.682 53.3751 4.90105 53.413 5.15879C53.4508 5.41654 53.4904 5.636 53.5008 5.64646C53.5113 5.65693 53.7544 5.6931 54.0412 5.72688C54.7034 5.8049 54.7566 5.80685 54.7566 5.75279C54.7566 5.70722 53.9573 0.897449 53.9281 0.766995C53.9128 0.698753 53.8596 0.688163 53.1611 0.614136C52.7483 0.570386 52.3858 0.527838 52.3555 0.519602ZM52.9502 1.63444C52.9547 1.63436 52.9558 1.64691 52.9564 1.67203C52.9564 1.7026 53.013 2.15048 53.0809 2.66731C53.1488 3.18414 53.2044 3.62349 53.2044 3.64362C53.2044 3.66374 53.1687 3.67824 53.125 3.67586C52.989 3.66847 52.2013 3.57075 52.1904 3.55994C52.1786 3.54803 52.8734 1.75647 52.9225 1.67258C52.9374 1.64721 52.9456 1.63451 52.9501 1.63444H52.9502Z"
                                fill="gray"
                              />
                              <path
                                d="M36.7178 5.31219C36.5846 5.04893 34.7461 0.642484 34.7627 0.625948C34.7731 0.615449 35.0787 0.573635 35.4417 0.533043C35.9453 0.476717 36.1077 0.469183 36.1275 0.50121C36.1418 0.524296 36.4537 1.37617 36.8208 2.39429C37.2572 3.60479 37.4964 4.22392 37.5121 4.18329C37.5254 4.14913 37.6569 3.25736 37.8046 2.20159C37.9523 1.14582 38.0798 0.271209 38.088 0.258021C38.1072 0.226742 39.2996 0.100045 39.326 0.126461C39.3463 0.146837 38.3884 5.1395 38.3535 5.19464C38.3434 5.21098 38.0173 5.25852 37.6292 5.30031C37.2412 5.34211 36.8884 5.3838 36.8454 5.39295C36.7865 5.40548 36.755 5.38556 36.7178 5.31219Z"
                                fill="gray"
                              />
                              <path
                                d="M47.15 5.09072C46.8177 4.95931 46.6065 4.67766 46.5588 4.30232C46.538 4.13703 46.5399 4.13151 46.6163 4.15327C47.1389 4.30228 47.2284 4.30875 47.4397 4.21281C47.6974 4.09578 47.6977 4.09403 47.7798 1.98689C47.8195 0.968245 47.8538 0.13324 47.8559 0.131321C47.8754 0.113682 49.0068 0.176541 49.0267 0.196373C49.0731 0.242804 48.9076 3.95368 48.8481 4.20168C48.7453 4.62999 48.5318 4.92163 48.221 5.05851C47.9552 5.17558 47.4063 5.19209 47.15 5.09072Z"
                                fill="gray"
                              />
                              <path
                                d="M41.3022 4.72972C41.2912 4.56965 41.2727 3.45046 41.2608 2.24264L41.2394 0.0465949H42.1362C42.6294 0.0465949 43.4404 0.0361276 43.9383 0.0233147L44.8436 0V0.481909V0.963817H44.3378C44.0595 0.963817 43.5198 0.974261 43.1383 0.987046L42.4447 1.01027V1.45447C42.4447 1.69878 42.4546 1.93337 42.4659 1.97579L42.4865 2.0529L43.4355 2.03199L44.3845 2.01107V2.49287V2.97466H43.6488C43.2442 2.97466 42.8156 2.98485 42.6963 2.99733L42.4795 3.01999V3.54983V4.07968L43.7142 4.05612L44.9489 4.03255V4.50339V4.97424L44.1816 4.99741C43.7596 5.01017 42.9435 5.02061 42.368 5.02067L41.3216 5.02073L41.3019 4.72969L41.3022 4.72972Z"
                                fill="gray"
                              />
                            </svg>
                          </div>
                          <div className="-mr-2">
                            <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-500">
                              <span className="sr-only">Close menu</span>
                              <XIcon className="h-6 w-6" aria-hidden="true" />
                            </Popover.Button>
                          </div>
                        </div>
                        <div className="mt-3 space-y-1 px-2">
                          {navigation.map((item) => (
                            <a
                              key={item.name}
                              href={item.href}
                              className="block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-100 hover:text-gray-800"
                            >
                              {item.name}
                            </a>
                          ))}
                          <Adminstration />
                        </div>
                      </div>
                      <div className="pt-4 pb-2">
                        <div className="flex items-center px-5">
                          <div className="flex-shrink-0">
                            <img
                              className="h-10 w-10 rounded-full"
                              src={user.imageUrl}
                              alt=""
                            />
                          </div>
                          <div className="ml-3 min-w-0 flex-1">
                            <div className="truncate text-base font-medium text-gray-800">
                              {user.name}
                            </div>
                            <div className="truncate text-sm font-medium text-gray-500">
                              {user.email}
                            </div>
                          </div>
                          <button
                            type="button"
                            className="ml-auto flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-cyan-500 focus:ring-offset-2"
                          >
                            <span className="sr-only">View notifications</span>
                            <BellIcon className="h-6 w-6" aria-hidden="true" />
                          </button>
                        </div>
                        <div className="mt-3 space-y-1 px-2">
                          {userNavigation.map((item) => (
                            <a
                              key={item.name}
                              href={item.href}
                              className="block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-100 hover:text-gray-800"
                            >
                              {item.name}
                            </a>
                          ))}
                        </div>
                      </div>
                    </div>
                  </Popover.Panel>
                </Transition.Child>
              </div>
            </Transition.Root>

          </>
        )}
      </Popover>
      <Outlet />
    </>
  );
}
