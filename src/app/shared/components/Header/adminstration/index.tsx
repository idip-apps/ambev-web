import { Menu, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import { admNavigation } from "..";

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

export function Adminstration() {
  return (
    <Menu as="div" className="relative flex-shrink-0">
      <Menu.Button
        className="flex rounded-md text-sm focus:outline-none 
                       bg-white bg-opacity-0 font-medium hover:bg-opacity-10"
      >
        <span className="sr-only"></span>
        <span className="text-left block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-100 hover:text-gray-800">
          Administração
        </span>
      </Menu.Button>

      <Transition
        as={Fragment}
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items
          className="absolute  z-40 mt-2 w-full origin-top-right
                             rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 
                             focus:outline-none"
        >
          {admNavigation.map((item) => (
            <Menu.Item key={item.page}>
              {({ active }) => (
                <a
                  href={item.href}
                  className={classNames(
                    active ? "bg-gray-100" : "",
                    "block px-4 py-2 text-sm text-gray-700"
                  )}
                >
                  {item.page}
                </a>
              )}
            </Menu.Item>
          ))}
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
