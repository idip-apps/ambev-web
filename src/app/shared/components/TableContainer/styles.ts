import tw from "tailwind-styled-components";


export const Container = tw.div`
  mt-5
  -mb-2 
  overflow-x-auto 
  sm:-mx-6 
  lg:-mx-8
`;
export const OverflowSeparator = tw.div`
  py-2 
  align-middle 
  inline-block 
  min-w-full 
  sm:px-6 
  lg:px-8
`;
export const TableWrapper = tw.div`
  shadow 
  overflow-hidden 
  border-b 
  border-gray-200 
  sm:rounded-lg
`;
export const Table = tw.table`
  min-w-full 
  divide-y 
  divide-gray-200
`;
export const Thead = tw.thead`
  bg-gray-50
`;
export const Th = tw.th`
  px-6 
  py-3
  text-left
  text-xs 
  font-medium 
  text-gray-500 
  uppercase 
  tracking-wider"
`;
export const ThSrOnly = tw.th`
  relative 
  px-6 
  py-3
`;
export const TBody = tw.tbody`
  bg-white 
  divide-y 
  divide-gray-200
`;
