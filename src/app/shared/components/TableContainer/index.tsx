import React from 'react';

import { Container, OverflowSeparator, TableWrapper, Table, Thead, Th, ThSrOnly, TBody } from './styles';
interface ITableContainer {
  headers: string[]
  SrOnlyHeaders: string[]
}

const TableContainer: React.FC<ITableContainer> = ({ children, headers, SrOnlyHeaders }) => {
  return (
    <Container>
      <OverflowSeparator>
        <TableWrapper>
          <Table>
            <Thead >
              <tr>
                {headers.map(header => (
                  <Th scope="col">
                    {header}
                  </Th>
                ))}
                {SrOnlyHeaders.map(SrOnlyHeader => (
                  <ThSrOnly scope="col">
                    <span className="sr-only">{SrOnlyHeader}</span>
                  </ThSrOnly>
                ))}
              </tr>
            </Thead>
            <TBody>
              {children}
            </TBody>
          </Table>
        </TableWrapper>
      </OverflowSeparator>
    </Container>
  )
}

export default TableContainer;