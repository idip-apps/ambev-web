import tw from "tailwind-styled-components"

type TShownValue = { $placeholderstyle: string }
type TContainerCheckIcon = { $active: boolean }
type TOptionText = { $selected: boolean }

export const style_Label = `
  text-sm 
  font-medium 
  text-gray-700 
  flex 
  items-center 
  justify-between
`

export const InputContainer = tw.div`
  mt-1 
  relative
`
export const style_Button = `
  relative 
  bg-white 
  border 
  min-h-[37px] 
  border-gray-300 
  rounded-md 
  shadow-sm 
  pl-3 
  pr-10 
  py-2 
  text-left 
  cursor-default 
  focus:outline-none 
  focus:ring-1 
  focus:ring-indigo-500 
  focus:border-indigo-500 
  text-sm md:text-base
`

export const ShownValue = tw.span<TShownValue>`
  ${({ $placeholderstyle }) => $placeholderstyle}
  block truncate
`
export const IconContainer = tw.span`
  absolute 
  inset-y-0 
  right-0 
  flex 
  items-center 
  pr-2 
  pointer-events-none
`
export const style_SelectorIcon = `
  h-5 
  w-5 
  text-gray-400
`
export const style_Options = `
  absolute 
  z-10 
  mt-1 
  w-full 
  bg-white 
  shadow-lg 
  max-h-60 
  rounded-md 
  py-1 
  text-base 
  ring-1 
  ring-black 
  ring-opacity-5 
  overflow-auto 
  focus:outline-none 
  sm:text-sm  
`
export const style_Option = `
  cursor-default 
  select-none 
  relative 
  py-2 
  pl-3 
  pr-9 
  min-h-[37px]
`
export const style_OptionActive = `
  text-white 
  bg-indigo-600
`
export const style_OptionInative = `
  text-gray-900
`
export const OptionText = tw.span<TOptionText>`
${({ $selected }) => $selected ? 'font-semibold' : 'font-normal'}
  block 
  truncate
`

export const ContainerCheckIcon = tw.span<TContainerCheckIcon>`
  ${({ $active }) => $active ? 'font-semibold' : 'text-indigo-600'}
  absolute 
  inset-y-0 
  right-0 
  flex 
  items-center 
  pr-4
`
export const style_CheckIcon = `
  h-5 
  w-5
`




// .listBoxButton{
//   @apply relative bg-white border min-h-[37px] border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 text-sm md:text-base
// }

// .select_label{
//   @apply  text-sm font-medium text-gray-700 flex items-center justify-between
// }

// .listBoxOptions{
//   @apply absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm
// }

// .listBoxOption{
//   @apply cursor-default select-none relative py-2 pl-3 pr-9 min-h-[37px]
// }

// .listBoxOption_active{
//   @apply text-white bg-indigo-600
// }

// .listBoxOption_inative{
//   @apply text-gray-900
// }

// .listBoxText{
//   @apply block truncate
// }

// .listBoxText_selected{
//   @apply font-semibold
// }

// .listBoxText_unselected{
//   @apply font-normal
// }

// .listBoxCheckIcon{
//   @apply absolute inset-y-0 right-0 flex items-center pr-4
// }
// .listBoxCheckIcon_selected{
//   @apply font-semibold
// }

// .listBoxCheckIcon_unselected{
//   @apply text-indigo-600
// }

