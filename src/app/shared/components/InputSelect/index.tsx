import { Listbox, Transition } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/outline';
import React, { Fragment, useLayoutEffect, useState } from 'react';
import { ContainerCheckIcon, IconContainer, InputContainer, OptionText, ShownValue, style_Button, style_CheckIcon, style_Label, style_Option, style_OptionActive, style_OptionInative, style_Options, style_SelectorIcon } from './styles';

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

interface IInputSelectProps {
  title: string
  data: { id: number, dataValue: string }[]
  filterValue: string
  setFilterValue: React.Dispatch<React.SetStateAction<string>>
  placeholder?: string
  optional?: boolean
  classSizes?: string
}

const InputSelect: React.FC<IInputSelectProps> = ({ setFilterValue, filterValue, data, title, placeholder, optional, classSizes }) => {

  const [inputShow, setInputShow] = useState<string>(filterValue)
  const [placeholderStyle, setPlaceholderStyle] = useState<string>('text-gray-400')
  const sizes = classSizes || 'ssm:w-96  sssm:w-56 w-48'

  const handleValueChange = (value: string) => {
    if (!value && placeholder) {
      setInputShow(placeholder)
      setPlaceholderStyle('text-gray-400')
      return
    }
    setFilterValue(value)
    setInputShow(value)
    setPlaceholderStyle('')
  }

  useLayoutEffect(() => {
    if (placeholder && !filterValue) {
      setInputShow(placeholder)
    }
  }, [])
  return (
    <div className="mt-5">
      <Listbox value={filterValue} onChange={handleValueChange}>
        {({ open }) => (
          <>
            <Listbox.Label className={style_Label}>
              {title}
              {optional && (<span>(Opcional)</span>)}
            </Listbox.Label>
            <InputContainer>
              <Listbox.Button className={`${style_Button} ${sizes}`}>
                <ShownValue $placeholderstyle={placeholderStyle}>
                  {inputShow}
                </ShownValue>
                <IconContainer>
                  <SelectorIcon
                    className={style_SelectorIcon}
                    aria-hidden="true"
                  />
                </IconContainer>
              </Listbox.Button>

              <Transition
                show={open}
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Listbox.Options className={style_Options}>
                  {data.map(({ id, dataValue }) => (
                    <Listbox.Option
                      key={id}
                      className={({ active }) =>
                        classNames(
                          style_Option,
                          active ?
                            style_OptionActive :
                            style_OptionInative
                        )
                      }
                      value={dataValue}
                    >
                      {({ selected, active }) => (
                        <>
                          <OptionText $selected={selected}>
                            {dataValue}
                          </OptionText>

                          {selected && (
                            <ContainerCheckIcon $active={active}>
                              <CheckIcon
                                className={style_CheckIcon}
                                aria-hidden="true"
                              />
                            </ContainerCheckIcon>
                          )}
                        </>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </InputContainer>
          </>
        )}
      </Listbox>
    </div>)
}

export default InputSelect;