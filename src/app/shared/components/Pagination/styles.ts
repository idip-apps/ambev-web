import tw from "tailwind-styled-components"

export const Nav = tw.nav`
  border-t 
  border-gray-300
  px-4 
  flex 
  items-center 
  justify-between 
  sm:px-0 mt-4
`
export const PreviousPageContainer = tw.div`
  -mt-px 
  w-0 
  flex-1 
  flex 
  
`
export const NextPageContainer = tw.div`
  -mt-px 
  w-0 
  flex-1 
  flex 
  justify-end
`
export const ChangePageButton = tw.button`
  border-t-2 
  border-transparent 
  pt-4
  pr-1 
  inline-flex 
  items-center
  text-sm 
  font-medium 
  text-gray-500
  hover:text-gray-700 
  hover:border-gray-500
`
export const style_ArrowIcon = `
  h-5 
  w-5 
  text-gray-400
`
export const style_PageNumber = `
  border-transparent
  text-gray-500
  hover:text-gray-700
  hover:border-gray-500
  border-t-2 
  pt-4 
  px-4 
  inline-flex 
  items-center 
  text-sm 
  font-medium
`
export const style_PageCurrent = `
  border-indigo-500 
  text-indigo-600 
  border-t-2 
  pt-4 
  px-4 
  inline-flex 
  items-center
  text-sm font-medium
`

export const Dots = tw.span`
  border-transparent 
  text-gray-500 
  border-t-2 
  pt-4 
  px-4 
  inline-flex 
  items-center 
  text-sm 
  font-medium
`




// .nav{
//   @apply border-t border-gray-300 px-4 flex items-center justify-between sm:px-0 mt-4
// }

// .divArrow{
//   @apply -mt-px w-0 flex-1 flex justify-end
// }

// .linkArrow{
//   @apply border-t-2 border-transparent pt-4 pr-1 inline-flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-500
// }
// .iconArrow{
//   @apply  h-5 w-5 text-gray-400
// }

// .pageNumber{
//   @apply border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-500
//   border-t-2 pt-4 px-4 inline-flex items-center text-sm font-medium
// }
// .pageCurrent{
//   @apply border-indigo-500 text-indigo-600 border-t-2 pt-4 px-4 inline-flex items-center
//    text-sm font-medium
// }

// .dots{
//   @apply border-transparent text-gray-500 border-t-2 pt-4 px-4 inline-flex items-center text-sm font-medium
// }