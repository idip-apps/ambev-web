import tw from "tailwind-styled-components";

export const Container = tw.div`
  mt-5 
  flex 
  justify-end
`
export const LinkButton = tw.a`
  flex 
  w-48 
  items-center 
  justify-center
  rounded-md 
  border 
  border-gray-300 
  bg-indigo-500 
  px-10 
  py-3 
  text-sm 
  font-medium
 text-white 
 shadow-sm 
 hover:bg-indigo-600"
`