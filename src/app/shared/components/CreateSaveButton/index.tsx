import React from 'react';
import { Container, LinkButton } from './styles';


interface ICreateSaveButtonProps {
  create: boolean
  type: string
}

const CreateSaveButton: React.FC<ICreateSaveButtonProps> = ({ create, type }) => {
  const textCreate = `Criar ${type}`
  const textEdit = 'Salvar Alterações'
  return (
    <Container>
      <LinkButton href="/" >
        {`${create ? textCreate : textEdit}`}
      </LinkButton>
    </Container>
  )
}

export default CreateSaveButton;