import React from 'react';
import { FiArrowLeftCircle } from 'react-icons/fi';
import { useNavigate } from 'react-router-dom';
import { ButtonContainer, Container, Title, Button, ButtonText, style_Icon } from './styles';

type TDetailsCardHeader = {
  texts: string[]
  createNew: boolean
}

const DetailsCardHeader: React.FC<TDetailsCardHeader> = ({ createNew, texts }) => {
  const navigate = useNavigate()
  const textCreateNew = texts[0]
  const textEdit = texts[1]

  return (
    <Container>
      <Title>
        {createNew ? textCreateNew : textEdit}
      </Title>
      <ButtonContainer>
        <Button onClick={() => navigate(-1)}>
          <ButtonText>Voltar</ButtonText>
          <FiArrowLeftCircle size="23" className={style_Icon} />
        </Button>
      </ButtonContainer>
    </Container >)
}

export default DetailsCardHeader;