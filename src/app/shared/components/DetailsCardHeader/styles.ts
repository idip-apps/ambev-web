import tw from "tailwind-styled-components";

export const Container = tw.div`
  flex 
  justify-between 
`

export const Title = tw.h1`
  text-black 
  font-bold 
  text-2xl
`
export const ButtonContainer = tw.div`
  mb-4
`
export const Button = tw.button`
  flex 
  w-full 
  items-center 
  justify-center 
  rounded-md
  sssm:border 
  border-gray-300 
  bg-none px-3 
  ssm:px-5 
  py-3 
  text-sm 
  font-medium
  text-gray-800 
  shadow-sm 
  hover:bg-gray-200
`
export const ButtonText = tw.span`
  sssm:flex 
  hidden
`
export const style_Icon = `
  text-gray-400 
  ml-3
`




// .backButton {
//   @apply flex w-full items-center justify-center rounded-md
//   sssm:border border-gray-300 bg-none px-3 ssm:px-5 py-3 text-sm font-medium
//    text-gray-800 shadow-sm hover:bg-gray-200
// }