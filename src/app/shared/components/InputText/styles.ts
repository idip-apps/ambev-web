import tw from "tailwind-styled-components";

type TInput = { $sizes: string }

export const Container = tw.div`
  mt-5
`
export const Label = tw.div`
  block 
  text-sm 
  font-medium 
  text-gray-700
`

export const Input = tw.input<TInput>`
  ${({ $sizes }) => $sizes}
  bg-white 
  mt-1 
  relative 
  border 
  border-gray-300 
  rounded-md
  shadow-sm 
  pl-3 
  pr-3 
  py-2 
  text-left 
  cursor-default 
  focus:outline-none 
  h-[37px] 
  focus:ring-1 
  focus:ring-indigo-500 
  focus:border-indigo-500 
  text-sm  
  md:text-base
`