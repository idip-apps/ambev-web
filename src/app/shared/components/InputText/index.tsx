import React, { ChangeEvent, HTMLAttributes } from 'react';
import { Container, Input, Label } from './styles';

interface IInputText extends HTMLAttributes<HTMLInputElement> {
  handleInputChange?: (e: ChangeEvent) => void
  label: string
  placeholder?: string
  classSizes?: string
}
type TRef = HTMLInputElement

const InputText = React.forwardRef<TRef, IInputText>(({ handleInputChange, label, placeholder, classSizes, defaultValue }, ref) => {

  return (
    <Container>
      <Label >
        {label}
      </Label>
      <Input
        $sizes={classSizes || 'ssm:w-96  sssm:w-56 w-48'}
        ref={ref}
        placeholder={placeholder || ''}
        onChange={handleInputChange}
        defaultValue={defaultValue}
      />
    </Container>)
})

export default InputText;