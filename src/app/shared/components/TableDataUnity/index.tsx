import React from 'react';
import { TUnity } from '../../MOCK/unities2';
import { City, Unity } from './styles';

type TTableDataUnity = { unity: TUnity }

const TableDataUnity: React.FC<TTableDataUnity> = ({ unity }) => {
  return (
    <>
      <Unity>
        {unity.unity}
      </Unity>
      <City>
        {unity.city}
      </City>
    </>
  )
}

export default TableDataUnity;