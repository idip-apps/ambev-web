import tw from "tailwind-styled-components";

export const Unity = tw.div` 
  text-sm 
  text-gray-900
  font-medium
`
export const City = tw.div` 
  text-sm 
  text-gray-500
`