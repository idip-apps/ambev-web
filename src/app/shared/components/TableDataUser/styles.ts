import tw from 'tailwind-styled-components';

export const Container = tw.div`
  flex 
  items-center
`;
export const ImgContainer = tw.div`
  flex-shrink-0 
  h-10 
  w-10
`;
export const Image = tw.img`
  h-10 
  w-10 
  rounded-full
`;
export const InfoContainer = tw.div`
  ml-4
`;
export const Name = tw.div`
  text-sm 
  font-medium 
  text-gray-900
`;
export const Email = tw.div`
  text-sm 
  text-gray-500
`;
