import React from 'react';
import { TUser } from '../../MOCK/users2';

import { Container, Email, Image, ImgContainer, InfoContainer, Name } from './styles';
interface ITableDataUser {
  user: TUser
}

const TableDataUser: React.FC<ITableDataUser> = ({ user }) => {
  return (
    <Container>
      <ImgContainer>
        <Image src={user.image} alt={user.name} />
      </ImgContainer>
      <InfoContainer>
        <Name>
          {user.name}
        </Name>
        <Email>
          {user.email}
        </Email>
      </InfoContainer>
    </Container>
  )
}

export default TableDataUser;