import React from 'react';
import { ButtonContainer, Container, LinkButton, Title } from './styles';

interface ICrudCardHeader {
  title: string
  createNewHref: string
}

const CrudCardHeader: React.FC<ICrudCardHeader> = ({ createNewHref, title }) => {
  return (
    <Container>
      <Title>{title}</Title>
      <ButtonContainer>
        <LinkButton href={createNewHref}>
          Novo
        </LinkButton>
      </ButtonContainer>
    </Container>)
}

export default CrudCardHeader;