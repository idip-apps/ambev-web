import tw from "tailwind-styled-components";

export const Container = tw.div`
  flex 
  justify-between 
`
export const Title = tw.div`
  text-black 
  font-bold 
  text-2xl
`
export const ButtonContainer = tw.div`
  mb-6
`
export const LinkButton = tw.a`
  flex 
  w-full 
  items-center 
  justify-center 
  rounded-md
  border 
  border-gray-300 
  bg-indigo-500 
  px-10
  py-3 
  text-sm 
  font-medium
  text-white 
  shadow-sm 
  hover:bg-indigo-600
`


/* .createNewButton{
  @apply flex w-full items-center justify-center rounded-md 
  border border-gray-300 bg-indigo-500 px-10 py-3 text-sm font-medium
   text-white shadow-sm hover:bg-indigo-600
} */