import React from 'react';
import { BsCheck2 } from 'react-icons/bs';
import { Button, ButtonContainer, Filters, Line, LineContainer, Title, TitleContainer, Wrapper, Container } from './styles';

interface IFilterContainer {
  filterChecked: boolean
  setFilterChecked: React.Dispatch<React.SetStateAction<boolean>>
}

const FilterContainer: React.FC<IFilterContainer> = ({ children, filterChecked, setFilterChecked }) => {
  return (
    <Container>
      <Wrapper>
        <LineContainer aria-hidden="true">
          <Line />
        </LineContainer>
        <TitleContainer>
          <Title>
            Selecione e Aplique os filtros
          </Title>
        </TitleContainer>
      </Wrapper>
      <Filters>

        {children}

        <ButtonContainer>
          <Button
            onClick={() => setFilterChecked(!filterChecked)}>
            <BsCheck2 size={30} className="ml-1" />
          </Button>
        </ButtonContainer>

      </Filters>
    </Container >
  )
}

export default FilterContainer;