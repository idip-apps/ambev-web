import tw from "tailwind-styled-components";

export const Container = tw.div`
  p-3
  sssm:p-5 
  bg-sky-50 
  rounded-lg 
  flex-col
`
export const Wrapper = tw.div`
  relative
`
export const LineContainer = tw.div`
  absolute 
  inset-0 
  flex 
  items-center
`
export const Line = tw.div`
  w-full 
  border-t 
  border-gray-300
`
export const TitleContainer = tw.div`
  relative 
  flex 
  items-center 
  justify-between
`
export const Title = tw.span`
  pr-3 
  bg-sky-50 
  text-sm 
  font-medium 
  text-gray-500 
  uppercase
`
export const Filters = tw.div`
  flex 
  items-end 
  gap-4 
  flex-wrap
`
export const ButtonContainer = tw.div`
  mt-5
`
export const Button = tw.button`
 bg-indigo-500 hover:bg-indigo-400 text-white
  w-10 
  h-9 
  block 
  rounded-md 
  shadow-sm 
  border
`









// .filter_title{
//   @apply pr-3 bg-sky-50 text-sm font-medium text-gray-500 uppercase
// }

// .filters{
//   @apply flex items-end gap-4 flex-wrap
// }