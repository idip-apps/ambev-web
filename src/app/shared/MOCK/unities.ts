export const UNITIES = [
  {
    id: "1",
    unity: "Cervejaria Barra",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "Danila Bonfin de Nascimento",
    email: "Danila.bonfin@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: "2",
    unity: "Cervejaria Cascadura",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "Michel Amaral Rocha",
    email: "michel.rocha@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
  {
    id: "3",
    unity: "Cervejaria Botafogo",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "Andréia Vieira Lópes",
    email: "andreia.lopes@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
  {
    id: "4",
    unity: "Cervejaria Carioca 01",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "Mariana Oliveira",
    email: "mariana.oliveira@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=60",
  },
  {
    id: "5",
    unity: "Cervejaria Carioca 02",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "Kelly Leitão",
    email: "kelly.leitao@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1520813792240-56fc4a3765a7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: "7",
    unity: "Cervejaria Copacabana",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "José Cunha Braga",
    email: "jose.cunha@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80",
  },
  {
    id: "8",
    unity: "Cervejaria Flamengo",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "Fabiano Albuque",
    email: "Fabiano.Albuque@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
  },
  {
    id: "9",
    unity: "Cervejaria Itaboral",
    state: "RJ",
    city: "Rio de Janeiro",
    name: "Davi Bastos Justom",
    email: "davi.bastos@ambev.com.br",
    image:
      "https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
];
