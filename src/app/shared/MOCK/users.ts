export const USERS = [
  {
    id: 1,
    name: "Jane Cooper",
    email: "jane.cooper@example.com",
    unity: "Cervejaria Rio de Janeiro",
    city: "Rio de Janeiro - RJ",
    role: "Admin",
    image:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 2,
    name: "Danila Bonfin de Nascimento",
    email: "Danila.bonfin@ambev.com.br",
    unity: "Cervejaria Rio de Janeiro",
    city: "Rio de Janeiro - RJ",
    role: "Gerente de Gente",
    image:
      "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
  {
    id: 3,
    name: "Michel Amaral Rocha",
    email: "michel.rocha@ambev.com.br",
    unity: "Cervejaria São Paulo",
    city: "Rio de Janeiro - RJ",
    role: "Usuário",
    image:
      "https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
  {
    id: 4,
    name: "Andréia Vieira Lópes",
    email: "andreia.lopes@ambev.com.br",
    unity: "Cervejaria Rio de Janeiro",
    city: "Rio de Janeiro - RJ",
    role: "Admin",
    image:
      "https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=60",
  },
  {
    id: 5,
    name: "Mariana Oliveira",
    email: "mariana.oliveira@ambev.com.br",
    unity: "Cervejaria Rio de Janeiro",
    city: "Rio de Janeiro - RJ",
    role: "Gerente de Gente",
    image:
      "https://images.unsplash.com/photo-1520813792240-56fc4a3765a7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 6,
    name: "Kelly Leitão",
    email: "kelly.leitao@ambev.com.br",
    unity: "Cervejaria Rio de Janeiro",
    city: "Rio de Janeiro - RJ",
    role: "Analista de Gente",
    image:
      "https://images.unsplash.com/photo-1498551172505-8ee7ad69f235?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 7,
    name: "José Cunha Braga",
    email: "jose.cunha@ambev.com.br",
    unity: "Cervejaria Rio de Janeiro",
    city: "Rio de Janeiro - RJ",
    role: "Admin",
    image:
      "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
  {
    id: 8,
    name: "Fabiano Albuque",
    email: "Fabiano.Albuque@ambev.com.br",
    unity: "Cervejaria Rio de Janeiro",
    city: "Rio de Janeiro - RJ",
    role: "Admin",
    image:
      "https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
];
