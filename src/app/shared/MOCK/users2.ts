export const PROFILES_SELECT = [
  { id: 0, dataValue: "" },
  { id: 1, dataValue: "Usuário" },
  { id: 2, dataValue: "Admin" },
  { id: 3, dataValue: "Gerente de Gente" },
  { id: 4, dataValue: "Analista de Gente" },
]

export type TUser = {
  id: number,
  name: string,
  unity: number,
  role: string,
  email: string,
  front: number,
  image: string,
}

export const USERS: TUser[] = [
  {
    id: 1,
    name: "Danila Bonfim do Nascimento",
    unity: 1,
    role: PROFILES_SELECT[1].dataValue,
    email: "danila.bonfim@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 2,
    name: "Michel Amaral Rocha",
    unity: 2,
    role: PROFILES_SELECT[3].dataValue,
    email: "michel.amaral@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
  {
    id: 3,
    name: "Andréia Vieira Lopes",
    unity: 3,
    role: PROFILES_SELECT[4].dataValue,
    email: "andreia.lopes@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
  {
    id: 4,
    name: "Mariana Oliveira",
    unity: 4,
    role: PROFILES_SELECT[2].dataValue,
    email: "mariana.oliveira@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 5,
    name: "Kelly Leitão Camargo",
    unity: 5,
    role: PROFILES_SELECT[1].dataValue,
    email: "kelly.camargo@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 6,
    name: "José Cunha Braga",
    unity: 6,
    role: PROFILES_SELECT[3].dataValue,
    email: "jose.braga@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1520813792240-56fc4a3765a7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 7,
    name: "Fabiano Albuquerque Nogueira",
    unity: 7,
    role: PROFILES_SELECT[1].dataValue,
    email: "fabiano.nogueira@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1498551172505-8ee7ad69f235?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
  },
  {
    id: 8,
    name: "Davi Bastos Justo",
    unity: 0,
    role: PROFILES_SELECT[1].dataValue,
    email: "davi.justo@ambev.com.br",
    front: 1,
    image:
      "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=60",
  },
];


const MANAGERS_SELECT = USERS.filter(user => user.role === 'Gerente de Gente').map(({ id, name }) => {
  return { id, dataValue: name }
})
MANAGERS_SELECT.unshift({ id: 0, dataValue: '' })

export { MANAGERS_SELECT }