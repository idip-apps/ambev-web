
export type TUnity = {
  id: number,
  unity: string,
  state: string,
  city: string,
  manager: number
}

export const UNITIES: TUnity[] = [
  {
    id: 1,
    unity: "Cervejaria Barra",
    state: "RJ",
    city: "Rio de Janeiro",
    manager: 0
  },
  {
    id: 2,
    unity: "Cervejaria Cascadura",
    state: "RJ",
    city: "Rio de Janeiro",
    manager: 0
  },
  {
    id: 3,
    unity: "Cervejaria Botafogo",
    state: "RJ",
    city: "Rio de Janeiro",
    manager: 0
  },
  {
    id: 4,
    unity: "Cervejaria Salvador",
    state: "BA",
    city: "Salvador",
    manager: 0
  },
  {
    id: 5,
    unity: "Cervejaria Fortaleza",
    state: "CE",
    city: "Fortaleza",
    manager: 0
  },
  {
    id: 7,
    unity: "Cervejaria Cuiabá",
    state: "GO",
    city: "Goiás",
    manager: 0
  },
  {
    id: 8,
    unity: "Cervejaria São Carlos",
    state: "SP",
    city: "São Carlos",
    manager: 0
  },
  {
    id: 9,
    unity: "Cervejaria Maringá",
    state: "PR",
    city: "Rio de Janeiro",
    manager: 0
  },
];

const UNITIES_SELECT = UNITIES.map(({ id, unity }) => { return { id, dataValue: unity } })
UNITIES_SELECT.unshift({ id: 0, dataValue: '' })

export const CITIES_SELECT = [...new Set(UNITIES.map(({ city }) => city))].map((city, index) => ({ id: index, dataValue: city }))
export const STATES_SELECT = [...new Set(UNITIES.map(({ state }) => state))].map((state, index) => ({ id: index, dataValue: state }))

export { UNITIES_SELECT }