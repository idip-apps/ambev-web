export type TFront = {
  id: number,
  name: string,
  members: number[],
  manager: number,
}

export const FRONTS: TFront[] = [
  {
    id: 1,
    name: "Eventos e Família",
    members: [0, 1],
    manager: 5,
  },
  {
    id: 2,
    name: "Médico da Familia",
    members: [0, 1],
    manager: 3,
  },
  {
    id: 3,
    name: "Sala de Amamentação",
    members: [0, 1],
    manager: 6,
  },
  {
    id: 4,
    name: "Gympass",
    members: [0, 1],
    manager: 2,
  },
  {
    id: 5,
    name: "Regras de Whatsapp",
    members: [0, 1],
    manager: 5,
  },
  {
    id: 6,
    name: "Ambec",
    members: [0, 1],
    manager: 0,
  },
  {
    id: 7,
    name: "Pulses",
    members: [0, 1],
    manager: 1,
  },
  {
    id: 8,
    name: "Regras de Whatsapp e Telegram",
    members: [0, 1],
    manager: 5,
  },
];

const FRONTS_SELECT = FRONTS.map(({ id, name }) => { return { id, dataValue: name } })
FRONTS_SELECT.unshift({ id: 0, dataValue: '' })

export { FRONTS_SELECT }