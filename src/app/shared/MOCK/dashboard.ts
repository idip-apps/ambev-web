export const user = {
  name: "Mariana Oliveira",
  email: "Mariana.mari@example.com",
  unity: "Unidade Rio de Janeiro",
  role: "Gerente de Gente",
  imageUrl:
    "https://images.unsplash.com/photo-1500917293891-ef795e70e1f6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
};
export const navigation = [
  {
    name: "HEADER.HOME", href: "/home", current: true
  },
  { name: "HEADER.BEST_PRACTICES", href: "#", current: false },
  { name: "HEADER.NEWS", href: "#", current: false },
  { name: "HEADER.HELP", href: "#", current: false },
  // { name: "Administração", href: "#", current: false },
];
export const userNavigation = [
  { name: "HEADER.YOUR_PROFILE", href: "#" },
  { name: "HEADER.CONFIG", href: "#" },
  { name: "HEADER.LEAVE", href: "/" },
];