import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider } from "../shared/context/AuthProvider";
import { ProtectedLayout } from "../shared/context/ProtectedLayout";

import { LoginPage } from "../pages/LoginPage";
import { ForgetPassword } from "../pages/ForgetPassword";
import { NewPassword } from "../pages/NewPassword";
import { Dashboard } from "../pages/DashBoard";
import { Units } from "../pages/Units";
import { Users } from "../pages/Users";
import { Fronts } from "../pages/Fronts";
import { UnitsDetail } from "../pages/Units/UnitsDetail";
import { UserDetail } from "../pages/Users/UserDetail";
import { FrontsDetail } from "../pages/Fronts/FrontsDetail";
import { Header } from "../shared/components/Header";
import { AmbevI18nProvider, I18nProvider } from "../i18n";

export function AppRoutes() {
  return (
    <div className="App">
      <>
        <AmbevI18nProvider>
          <I18nProvider>
            <AuthProvider>
              <BrowserRouter>
                <Routes>
                  <Route path="/" element={
                    <ProtectedLayout>
                      <Header />
                    </ProtectedLayout>
                  } >
                    <Route path="/" element={<Dashboard />} />
                    <Route path="units" element={<Units />} />
                    <Route path="units-detail" element={<UnitsDetail />} />
                    <Route path="users" element={<Users />} />
                    <Route path="users-detail" element={<UserDetail />} />
                    <Route path="fronts" element={<Fronts />} />
                    <Route path="fronts-detail" element={<FrontsDetail />} />
                  </Route>
                  <Route path="/signin" element={<LoginPage />} />
                  <Route path="/forget-password" element={<ForgetPassword />} />
                  <Route path="/new-password" element={<NewPassword />} />
                </Routes>
              </BrowserRouter>
            </AuthProvider>
          </I18nProvider>
        </AmbevI18nProvider>
      </>
    </div>
  );
}
